<?php

/**
 * @author Saurabh Chhhabra <saurabhchhabra018@gmail.com>
 * @link http://localhost:8181/site/getapi Get all API's in Listed Format
 * @
 */

namespace app\models\modeloject;

final class ModelClass{
    
    public $modelObject = [];
    
    public function getModelObject() {
        return $this->modelObject;
    }

    /**
     * 
     * @param type $modelObject
     * @return $this
     */
    public function setModelObject($modelObject) {
        $this->modelObject[]=(object)$modelObject;
        return $this;
    }

    /**
     * 
     * @param type $modelObject
     */        
    public function model($modelObject){
        
        $this->setModelObject($modelObject);  
    }
        
    /**
     * @inheritdoc
     * @return static|null ActiveRecord instance matching the condition, or `null` if nothing matches.
     */
    public function count(){
        return count($this->modelObject);
    }
    
}
