<?php

/**
 * @author Saurabh Chhhabra <saurabhchhabra018@gmail.com>
 * @link http://localhost:8181/site/getapi Get all API's in Listed Format
 * @method Modal UserSession (type DBConnection) UserSession for generating User session history with Refresh Token
 * 
 */

namespace app\models;
use yii\db\ActiveRecord;

class UserSession extends ActiveRecord
{
 
    
    /**
     * 
     * @return string
     */
     public static function tableName(){
        return 'user_sessions';
    }
}
