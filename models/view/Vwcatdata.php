<?php

/**
 * @author Saurabh Chhhabra <saurabhchhabra018@gmail.com>
 * @link http://localhost:8181/site/getapi Get all API's in Listed Format
 * @method Modal UserMaster (type DBConnection) UserMaser for get/store user info
 * 
 */

namespace app\models\view;
use yii\db\ActiveRecord;

class Vwcatdata extends ActiveRecord
{
 
    
    /**
     * 
     * @return string
     */
     public static function tableName(){
        return 'vw_cat_data';
    }
}
