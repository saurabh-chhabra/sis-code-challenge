<?php

/**
 * @author Saurabh Chhhabra <saurabhchhabra018@gmail.com>
 * @link http://localhost:8181/site/getapi Get all API's in Listed Format
 * @method Modal UserMaster (type DBConnection) UserMaser for get/store user info
 * 
 */

namespace app\models;
use yii\db\ActiveRecord;

class ExpenseMaster extends ActiveRecord
{
 
    
    /**
     * 
     * @return string
     */
     public static function tableName(){
        return 'expense_master';
    }
}
