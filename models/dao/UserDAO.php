<?php

/**
 * @author Saurabh Chhhabra <saurabhchhabra018@gmail.com>
 * @link http://localhost:8181/site/getapi Get all API's in Listed Format
 * @
 */

namespace app\models\dao;

final class UserDAO {
    
    /**
     * @Entity name = user_master
     */
    
    private static $instance;
    /**
     * @var UserDAO $id
     */
    private $id;
    private $userId;
    private $fbId        = -1;
    private $gId         = -1;
    private $profilePic;
    private $fullName;
    private $emailId;
    private $password;
    private $gender;
    private $dob;
    private $roleName;
    private $sourceLogin;
    private $countryCode;
    private $phoneNumber;
    private $signupSteps = 0;
    private $byInviteCode;
    private $isEmailValid;
    private $isMobileValid;
    private $status;
    private $bio         = "";
    private $createdBy;
    private $updatedBy;
    private $createdDate;
    private $updatedDate;
    private $address1;
    private $address2;
    private $zipCode;
    private $state;
    private $city;
    private $country;
    private $deviceId;
    private $isDevice;
    private $homelessId;




    public function getId() {
        return $this->id;
    }

    public function getUserId() {
        return $this->userId;
    }

    public function getFbId() {
        return $this->fbId;
    }

    public function getGId() {
        return $this->gId;
    }

    public function getProfilePic() {
        return $this->profilePic;
    }

    public function getFullName() {
        return $this->fullName;
    }

    public function getEmailId() {
        return $this->emailId;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getSourceLogin() {
        return $this->sourceLogin;
    }

    public function getCountryCode() {
        return $this->countryCode;
    }

    public function getPhoneNumber() {
        return $this->phoneNumber;
    }

    public function getSignupSteps() {
        return $this->signupSteps;
    }

    public function getByInviteCode() {
        return $this->byInviteCode;
    }

    public function getIsEmailValid() {
        return $this->isEmailValid;
    }

    public function getIsMobileValid() {
        return $this->isMobileValid;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getCreatedBy() {
        return $this->createdBy;
    }

    public function getUpdatedBy() {
        return $this->updatedBy;
    }

    public function getCreatedDate() {
        return $this->createdDate;
    }

    public function getUpdatedDate() {
        return $this->updatedDate;
    }

    public function setId($id) {
        $this->id = (float)$id;
    }

    public function setUserId($userId) {
        $this->userId = (float)$userId;
    }

    public function setFbId($fbId) {
        $this->fbId = (float)$fbId;
    }

    public function setGId($gId) {
        $this->gId = (float)$gId;
    }

    public function setProfilePic($profilePic) {
        $this->profilePic = $profilePic;
    }

    public function setFullName($fullName) {
        $this->fullName = $fullName;
    }

    public function setEmailId($emailId) {
        $this->emailId = $emailId;
    }

    public function setPassword($password) {
        $this->password = md5($password);
    }

    public function setSourceLogin($sourceLogin) {
        $this->sourceLogin = $sourceLogin;
    }

    public function setCountryCode($countryCode) {
        $this->countryCode = $countryCode;
    }

    public function setPhoneNumber($phoneNumber) {
        $this->phoneNumber = (float)$phoneNumber;
    }

    public function setSignupSteps($signupSteps) {
        $this->signupSteps = (int)$signupSteps;
    }

    public function setByInviteCode($byInviteCode) {
        $this->byInviteCode = $byInviteCode;
    }

    public function setIsEmailValid($isEmailValid) {
        $this->isEmailValid = (int)$isEmailValid;
    }

    public function setIsMobileValid($isMobileValid) {
        $this->isMobileValid = (int)$isMobileValid;
    }

    public function setStatus($status) {
        $this->status = (int)$status;
    }

    public function setCreatedBy($createdBy) {
        $this->createdBy = $createdBy;
    }

    public function setUpdatedBy($updatedBy) {
        $this->updatedBy = $updatedBy;
    }

    public function setCreatedDate($createdDate) {
        $this->createdDate = $createdDate;
    }

    public function setUpdatedDate($updatedDate) {
        $this->updatedDate = $updatedDate;
    }

    public function getBio() {
        return $this->bio;
    }

    public function getCity() {
        return $this->city;
    }

    public function setBio($bio) {
        $this->bio = $bio;
        return $this;
    }

    public function setCity($city) {
        $this->city = $city;
        return $this;
    }
    
    public function getAddress1() {
        return $this->address1;
    }

    public function getAddress2() {
        return $this->address2;
    }

    public function getZipCode() {
        return $this->zipCode;
    }

    public function setAddress1($address1) {
        $this->address1 = $address1;
        return $this;
    }

    public function setAddress2($address2) {
        $this->address2 = $address2;
        return $this;
    }

    public function setZipCode($zipCode) {
        $this->zipCode = $zipCode;
        return $this;
    }

    public function getState() {
        return $this->state;
    }

    public function getCountry() {
        return $this->country;
    }

    public function setState($state) {
        $this->state = $state;
        return $this;
    }

    public function setCountry($country) {
        $this->country = $country;
        return $this;
    }

    public function getGender() {
        return $this->gender;
    }

    public function getDob() {
        return $this->dob;
    }

    public function setGender($gender) {
        $this->gender = $gender;
        return $this;
    }

    public function setDob($dob) {
        $this->dob = $dob;
        return $this;
    }
    
    public function getRoleName() {
        return $this->roleName;
    }

    public function setRoleName($roleName) {
        $this->roleName = $roleName;
        return $this;
    }

    public function getDeviceId() {
        return $this->deviceId;
    }

    public function getIsDevice() {
        return $this->isDevice;
    }

    public function setDeviceId($deviceId) {
        $this->deviceId = $deviceId;
        return $this;
    }

    public function setIsDevice($isDevice) {
        $this->isDevice = $isDevice;
        return $this;
    }
    
    public function getHomelessId() {
        return $this->homelessId;
    }

    public function setHomelessId($homelessId) {
        $this->homelessId = $homelessId;
        return $this;
    }

                    
    /**
     * @inheritdoc
     * @return static|null ActiveRecord instance matching the condition, or `null` if nothing matches.
     */
    public static function getInstance() {
        if (!(self::$instance instanceof self)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}

