<?php

/**
 * @author Saurabh Chhhabra <saurabhchhabra018@gmail.com>
 * @link http://localhost:8181/site/getapi Get all API's in Listed Format
 * @
 */

namespace app\models\dao;

use app\components\GlobalController as Globals;

final class UserProfileDAO {
    
    /**
     * @Entity name = user_master
     */
    
    public static $instance;
    
    public $userId      = -1;
    public $profilePic  = "";
    public $fullName    = "";
    public $emailId     = "";
    public $signupSteps = 0;
    public $phoneNumber = 0;
    public $city        = "";
    public $roleName    = "USER";
    public $gender      = "Male";
    public $dob         = "";
    public $status = 1;
    public $createdDate = null;
  

    public function getUserId() {
        return $this->userId;
    }

    public function getProfilePic() {
        return $this->profilePic;
    }

    public function getFullName() {
        return $this->fullName;
    }

    public function getEmailId() {
        return $this->emailId;
    }

    public function getSignupSteps() {
        return $this->signupSteps;
    }

    public function setUserId($userId) {
        $this->userId = (float)$userId;
        return $this;
    }

    public function setProfilePic($profilePic) {
//        if($profilePic!=NULL){
//            Globals::generateRandomInt(7);
//            $this->profilePic = Globals::$GETIMGAEURL.$profilePic.'?v='.Globals::generateRandomInt(7);
//            //$this->profilePic = "http://localhost:8282".$profilePic.'?v='.Globals::generateRandomInt(7);
//            return $this;
//        }
        
        if (strpos($profilePic, 'https://') !== false) {
            $this->profilePic = $profilePic;
            return $this;
        }
        
        $this->profilePic = Globals::$GETIMGAEURL.$profilePic.'?v='.Globals::generateRandomInt(7);
        return $this;
    }

    public function setEmailId($emailId) {
        $this->emailId = $emailId;
        return $this;
    }

    public function setSignupSteps($signupSteps) {
        $this->signupSteps = (int)$signupSteps;
        return $this;
    }

    public function getPhoneNumber() {
        return $this->phoneNumber;
    }

    public function setPhoneNumber($phoneNumber) {
        $this->phoneNumber = (float)$phoneNumber;
        return $this;
    }
    
    public function getRoleName() {
        return $this->roleName;
    }

    public function setRoleName($roleName) {
        $this->roleName = $roleName;
        return $this;
    }
    
    public function getGender() {
        return $this->gender;
    }

    public function setGender($gender) {
        $this->gender = $gender;
        return $this;
    }

    public function getDob() {
        return $this->dob;
    }

    public function setDob($dob) {
        $this->dob = $dob;
        return $this;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    public function getCreatedDate() {
        return $this->createdDate;
    }

    public function setCreatedDate($createdDate) {
        $this->createdDate = $createdDate;
        return $this;
    }

    function setFullName($fullName) {
        $this->fullName = $fullName;
    }

            
    /**
     * @inheritdoc
     * @return static|null ActiveRecord instance matching the condition, or `null` if nothing matches.
     */   
    public static function getInstance() {
        if (!(self::$instance instanceof self)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}

