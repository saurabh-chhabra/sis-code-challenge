<?php

/**
 * @author Saurabh Chhhabra <saurabhchhabra018@gmail.com>
 * @link http://localhost:8181/site/getapi Get all API's in Listed Format
 * @method Model Logs 
 * 
 */

namespace app\models;
use yii\db\ActiveRecord;

class Logs extends ActiveRecord
{
 
    
    /**
     * 
     * @return string
     */
     public static function tableName(){
        return 'request_response_logs';
    }
}
