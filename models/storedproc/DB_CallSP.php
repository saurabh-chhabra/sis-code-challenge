<?php

/**
 * @author Saurabh Chhhabra <saurabhchhabra018@gmail.com>
 * @link http://localhost:8181/site/getapi Get all API's in Listed Format
 * @method Stored Procedures
 * 
 */

namespace app\models\storedproc;

use yii;
use app\components\GlobalController as Globals;

class DB_CallSP {
 
    
    /**
     * 
     * @param type $foodType
     * @param type $carbsType
     * @param type $snackType
     * @return type
     */
     public static function sp_recipe_maker($foodType,$carbsType,$snackType){
        $result =   Yii::$app->db->createCommand("CALL sp_recipe_maker('$foodType','$carbsType','$snackType')") 
                    ->queryAll();  
        $_resultObj = Globals::convert_array_to_obj_recursive($result);
       
        return $_resultObj;
    }
    
    
    /**
     * 
     * @param type $lat
     * @param type $lng
     * @return type
     */
    public static function sp_near_by($lat,$lng){
        $result =   Yii::$app->db->createCommand("CALL usp_near_by('$lat','$lng')") 
                    ->queryAll();  
        $_resultObj = Globals::convert_array_to_obj_recursive($result);
       
        return $_resultObj;
    }
    
    public static function sp_get_my_txn($payTo){
        $result =   Yii::$app->db->createCommand("CALL getMyTxn('$payTo')") 
                    ->queryAll();  
        $_resultObj = Globals::convert_array_to_obj_recursive($result);
       
        return $_resultObj;
    }
    
}
