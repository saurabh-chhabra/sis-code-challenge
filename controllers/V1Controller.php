<?php

/**
 * 
 * @author Saurabh Chhabra <saurabhchhabra018@gmail.com>
 * @link http://localhost:8181/site/getapi Get all API's in Listed Format
 * 
 */


/* Master COntroller logs */
namespace app\controllers;
use app\components\GlobalController as Globals;
use app\components\AppException as AppException;

/*** Model Controllers **/

use app\models\Logs as Logs;

// date zone
date_default_timezone_set('Asia/Kolkata');

/* Defined Paths */
define ('APP_DIR','app\controllers');
define ('API_VERSION','v1');
define ('API_DIR_ADMIN','admin');
define ('API_DIR_DONAR','donar');
define ('API_DIR_MERCHANT','merchant');
define ('API_DIR_ORG','org');
define ('API_DIR_HOMELESS','homeless');
define ('API_DIR_VENDOR','vendor');
define ('API_DIR_GLOBAL','globalmod');
define ('API_DIR_USER','user\\roles');
define ('DIRECTORY_SEPARATORS','\\');

define ('WORLD_MAP_JSON',__DIR__.'/json/');
define ('CURRENCY_JSON','CurrencyFlip.json');
define ('CURRENCY_SYMBOL_JSON','CurrencySymbol.json');

define ('NAME_SPACE','use app\components\v1\messages\Messages as Messages');

class V1Controller extends \yii\web\Controller
{
   
    const API_DEFINED_PATH                  = APP_DIR.DIRECTORY_SEPARATORS.API_VERSION.DIRECTORY_SEPARATORS ;
    
    const API_DEFINED_PATH_ADMIN            = APP_DIR.DIRECTORY_SEPARATORS.API_VERSION.DIRECTORY_SEPARATORS.API_DIR_ADMIN.DIRECTORY_SEPARATORS ;
    
    const API_DEFINED_PATH_VENDOR           = APP_DIR.DIRECTORY_SEPARATORS.API_VERSION.DIRECTORY_SEPARATORS.API_DIR_VENDOR.DIRECTORY_SEPARATORS ;
    
    const API_DEFINED_PATH_GLOBAL           = APP_DIR.DIRECTORY_SEPARATORS.API_VERSION.DIRECTORY_SEPARATORS.API_DIR_GLOBAL.DIRECTORY_SEPARATORS ;
    
    const API_DEFINED_PATH_DONAR            = APP_DIR.DIRECTORY_SEPARATORS.API_VERSION.DIRECTORY_SEPARATORS.API_DIR_USER.DIRECTORY_SEPARATORS.API_DIR_DONAR.DIRECTORY_SEPARATORS ;
    const API_DEFINED_PATH_MERCHANT         = APP_DIR.DIRECTORY_SEPARATORS.API_VERSION.DIRECTORY_SEPARATORS.API_DIR_USER.DIRECTORY_SEPARATORS.API_DIR_MERCHANT.DIRECTORY_SEPARATORS ;
    const API_DEFINED_PATH_ORG              = APP_DIR.DIRECTORY_SEPARATORS.API_VERSION.DIRECTORY_SEPARATORS.API_DIR_USER.DIRECTORY_SEPARATORS.API_DIR_ORG.DIRECTORY_SEPARATORS ;
    const API_DEFINED_PATH_HOMELESS         = APP_DIR.DIRECTORY_SEPARATORS.API_VERSION.DIRECTORY_SEPARATORS.API_DIR_USER.DIRECTORY_SEPARATORS.API_DIR_HOMELESS.DIRECTORY_SEPARATORS ;
    
    public function actions()
    {
        
        @self::beforeActionRegisterlog();
        
        return [
            
                // global
               
                'userSignUp'            		=> self::API_DEFINED_PATH_GLOBAL.'login\SignUpAction',
                'getAllExpenses'                        => self::API_DEFINED_PATH.'user\GetAllExpensesAction',
                'userLogin'                             => self::API_DEFINED_PATH_GLOBAL.'login\LoginAction',  

                'analytics'                             => self::API_DEFINED_PATH.'user\AnalyticsAction',
               
			
        ];
    }
    
    
    public function beforeActionRegisterlog(){
        try{
            
            $_type                      = "Log Failure";
            
            $req                        = (file_get_contents('php://input'));
            $server                     = json_encode($_SERVER);         
            $url                        = $_SERVER['REQUEST_URI'];
            $userId                     = isset($_SERVER["HTTP_USERID"]) ? $_SERVER["HTTP_USERID"] : NULL;
            $ip                         = $_SERVER['REMOTE_ADDR'];
            
            //$block                      =$this->blockURL($url);
            
            $logId                      = @self::generateLogId('Logs');
            Globals::$LOG_ID            = $logId;
            
            $logs                       = new Logs();
            
            $logs->logRequestId         = $logId;
            $logs->url                  = $url;
            
            $logs->request              = utf8_encode($req);
            
            $logs->aasumeUserMasterId   = $userId;
            $logs->ipAddress            = $ip;
            $logs->serverVars           = $server;
            
            $logs->createdDate          = Globals::backCustomDate(0,'Logs');
            
            $logs->save();
            
        } catch (Exception $ex) {
            new AppException(!empty($ex->errorInfo[1])?$ex->errorInfo[1]:NULL, $_type);
            $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' => $_type, 'errorMessage' => $ex->getMessage());
            Globals::returnJsonResponse($_response);
            exit;
        } finally {
            
        }
    }
    
    /**
     * 
     * @param type $_type
     * @return type
     */
    public static function generateLogId($_type){
        try{
            
            $_random      =rand(5,15);
            
            $tempUserid   = Globals::generateRandomInt($_random);
            $user         = Logs::find()->where(["logRequestId"=>$tempUserid])->count();
            if($user>0){
                self::generateLogId($_type);
            }
            
            return $tempUserid;
            
        } catch (\Exception $ex) {
            new AppException(!empty($ex->errorInfo[1])?$ex->errorInfo[1]:NULL, $_type);
            $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' => $_type, 'errorMessage' => $ex->getMessage());
            Globals::returnJsonResponse($_response);
            exit;
        }
    }
}
