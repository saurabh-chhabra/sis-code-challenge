<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use app\components\GlobalController as Globals;

use app\models\UserMaster;
use app\models\ExpenseMaster;
use app\components\ValidatorsController;

class SiteController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
               
            ],
        ],
                
        ];
    }
    public function actionCreate() {

        return $this->render('index');
    }
    public function actionIndex() {

        return $this->render('index');
    }
    
    public function actionRegister() {

        return $this->render('register');
    }
    
    public function actionUpload() {
        
        $csv = array();
        
        // check there are no errors
        if (\Yii::$app->request->isPost) {
            $_res = ValidatorsController::checkUserSession('upload', Globals::ADMIN,1);
            if(!$_res['status']){
                throw new \Exception($_res['message']);
            }
            if($_FILES['file']['error'] == 0){                
                $name = $_FILES['file']['name'];
                $fileAtrr = explode('.',$name);
                
                $ext = $fileAtrr[1];
                $type = $_FILES['file']['type'];
                $tmpName = $_FILES['file']['tmp_name'];
                
                // check the file is a csv
                if($ext === 'psv'){
                    if(($handle = fopen($tmpName, 'r')) !== FALSE) {
                        // necessary if a large csv file
                        set_time_limit(0);

                        $row = 0;
                        $data = fgetcsv($handle, 1000, '|');
                        //print_r($data);die;
                        while( ($data = fgetcsv($handle, 1000, '|')) !== FALSE) {
                            // number of fields in the csv
                            $col_count = count($data);
                            if(count($data)!=7){
                                //print_r($data);die;
                                throw new \Exception("Please Upload (psv) file in correct column format.\n\n Error at Row Number ".($row+2));
                            }
                            // get the values from the csv
                            $date = new \DateTime($data[0]);
                            $csv[$row]['date'] = $date->format('Y-m-d H:i:s');
                            $csv[$row]['category'] = $data[1];
                            $csv[$row]['employee_id'] = UserMaster::find()->where(['emailId'=>$data[2]])->one()->userId;
                            $csv[$row]['employee_address'] = $data[3];
                            $csv[$row]['expense_description'] = $data[4];
                            $csv[$row]['pre_tax_amount'] = $data[5];
                            $csv[$row]['tax_amount'] = $data[6];

                            // inc the row
                            $row++;
                        }
                        fclose($handle);
                    }
                }
                
                
                foreach($csv as $data){
                    
                    $expense = new ExpenseMaster();
                    $expense->date = $data['date'];
                    $expense->category = $data['category'];
                    $expense->user_id = $data['employee_id'];
                    $expense->employee_address = $data['employee_address'];
                    $expense->expense_description = $data['expense_description'];
                    $expense->pre_tax_amount = $data['pre_tax_amount'];
                    $expense->tax_amount = $data['tax_amount'];
                    $expense->createdDate = Globals::customDate();
                    
                    $expense->save();
                }
                
                return $this->render('upload',['message'=>'Successfully Uploaded Expense Sheet']);
                echo "<pre>";print_r($csv);die;
            }
        }

        return $this->render('upload',['message'=>null]);
    }
    
    public function actionUser() {

        return $this->render('user');
    }

    public function actionExpense() {

        return $this->render('expense');
    }
    
    public function actionDashboard() {

        return $this->render('dashboard');
    }
    
    public function actionError() {
        $message = Yii::$app->errorHandler->exception->getMessage();
        
        return $this->render('error',['message'=>$message]);
        exit;
        
        echo "<pre>"; print_r(Yii::$app->errorHandler->exception);die;
        $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' => $_SERVER['REQUEST_URI'] . ' is Invalid Request', 'message' => null, 'errorMessage' => 'Invalid Request');
        Globals::returnJsonResponse($_response);
        exit;
    }
    
    public static function actionErrorPage() {
        $message = Yii::$app->errorHandler->exception->getMessage();
        
        return $this->render('error',['message'=>$message]);
        exit;
        
        echo "<pre>"; print_r(Yii::$app->errorHandler->exception);die;
        $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' => $_SERVER['REQUEST_URI'] . ' is Invalid Request', 'message' => null, 'errorMessage' => 'Invalid Request');
        Globals::returnJsonResponse($_response);
        exit;
    }

}
