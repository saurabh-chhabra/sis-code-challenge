<?php

/**
 * @author Saurabh Chhhabra <saurabhchhabra018@gmail.com>
 * @link http://localhost:8181/site/getapi Get all API's in Listed Format
 * @
 */
/**
 * Master Name-spaces
 */

namespace app\controllers;

namespace app\controllers\v1\globalmod\login;

/** Inherit Name-spaces * */
use \yii\web\Controller as BaseController;
use app\components\GlobalController as Globals;
use app\components\AppException as AppException;
use app\components\v1\messages\Messages as Messages;

/* * * Validations & Validators Controllers * */
use app\components\APIValidations as APIValidations;
use app\components\ValidatorsController as Validators;

/* * * Modal Controllers * */
use app\models\UserMaster as UserMaster;
use app\models\HomlessMaster;
use app\models\UserSession as UserSession;

/******** DAO *********/
use app\models\dao\UserDAO;
use app\models\dao\MADAO;
use app\models\MerchantAccount;
use app\models\modeloject\ModelClass as ModelClass;
/*** VENDOR **/
use app\controllers\v1\vendor\EmailAction as Email ;
use app\models\dao\HomelessDAO;
use app\models\dao\NotificationsDAO;

use app\models\dao\UserProfileDAO as UserProfileDAO;


final class SignUpAction extends Messages{

    public $_currDateTime;
    
    private $_data;
    private $flag=0;
    
    private $sucessMessage = self::successUserCreation;

    public function get_data(){
        return $this->_data;
    }

    public function set_data($_data){
        $this->_data = $_data;
    }

    public function getSucessMessage() {
        return $this->sucessMessage;
    }

    public function setSucessMessage($sucessMessage) {
        $this->sucessMessage = $sucessMessage;
        return $this;
    }

            
    public function runWithParams() {

        try {
            $_type = "SignUp";
            $contentType = Globals::APPLICATIONJSON;
            
            Validators::set_type($_type);

            $global = new Globals;
            $this->_currDateTime = $global->DATETIME;

            /**
             * Check Pre-Flight Headers
             * 
             * @step 1
             */
            Globals::corsHeader();
            Globals::checkHeader($contentType, $_type);
            Globals::checkRequest('POST', $_type);

            /**
             * Check API Validations for Required Keys
             * 
             * @step 2
             */
            /* @var $data type for JOSN Data */
            $data = \json_decode(file_get_contents('php://input'), true);

            $required = ['name','emailId','password'];
            /**
             * Check if Some Required Fields is Empty or Null
             * 
             * @step 3
             */
            $validate = APIValidations::checkMandatoryKeys($data, $required); //print_r($validate);die;

            if ($validate != 'ok') {
                $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' => Validators::get_type(), 'errorMessage' => APIValidations::$statusVar[$validate] . Globals::cannotBlank);
                Globals::returnJsonResponse($_response);
                exit;
            }
            
            /**
             * Check if Some Field is Not Defined Push into Final Array & make as NULL
             * 
             * @step 4
             */
            $jsonArray = ['name','phoneNumber','emailId','gender','dob', 'password','countryCode','roleName'];
            $data = Validators::checkIsNullExp($data, $jsonArray, Validators::get_type());
            
            $this->set_data($data);
            $this->DAO();
            
            $this->validationParams();
           
            /**
             * Play with Data
             * 
             * @step 5
             */
            $this->insertUserData();
        } catch (\Exception $ex) {
            new AppException(!empty($ex->errorInfo[1])?$ex->errorInfo[1]:NULL, Validators::get_type());
            $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' => Validators::get_type(), 'errorMessage' => $ex->getMessage());
            Globals::returnJsonResponse($_response);
            exit;
        }
    }
    
    /**
     *  @entity map to DAO
     */
    protected function DAO(){
        
        $userDAO = new UserDAO();
        $userDAO->setFullName($this->get_data()['name']);
        $userDAO->setEmailId($this->get_data()['emailId']);
        $userDAO->setPassword($this->get_data()['password']);
        $userDAO->setPhoneNumber($this->get_data()['phoneNumber']);
        $userDAO->setCountryCode($this->get_data()['countryCode']);
        //$userDAO->setDob($this->get_data()['dob']);
        $userDAO->setGender("MALE");
        $userDAO->setRoleName('USER');
        
        $this->set_data($userDAO);
        
        
    }

    /**
     * 
     * Validate params
     */
    protected function validationParams(){
        
        APIValidations::validateName($this->get_data()->getFullName(), Validators::get_type());
        //APIValidations::validateNumber($this->get_data()->getPhoneNumber(), Validators::get_type());
        
        
        Validators::stringType($this->get_data()->getFullName(), Globals::STRING_DATA_LEN_255, Validators::get_type());
        Validators::stringType($this->get_data()->getEmailId(), Globals::STRING_DATA_LEN_255, Validators::get_type());
        Validators::stringType($this->get_data()->getPassword(), Globals::STRING_DATA_LEN_255, Validators::get_type());
        Validators::stringType($this->get_data()->getPhoneNumber(), Globals::STRING_DATA_LEN_255, Validators::get_type());
        
        Validators::stringType($this->get_data()->getPhoneNumber(), Globals::STRING_DATA_LEN_255, Validators::get_type());
        Validators::stringType($this->get_data()->getPhoneNumber(), Globals::STRING_DATA_LEN_255, Validators::get_type());
        
        Validators::stringType($this->get_data()->getRoleName(), Globals::STRING_DATA_LEN_255, Validators::get_type());
        
    }
    
    
    /**
     * 
     * @throws \Exception
     */
    public static function checkEmailExists($data){
        $user = UserMaster::find()->where(['emailId'=>$data->getEmailId(),'status'=>'1'])->one();
        
        if(!empty($user)){
            throw new \Exception(self::userExistEmailId);
        }
    }
    
    public static function checkPhoneNumberExists($phoneNumber){
        if($phoneNumber==null){
            return;
        }
        $user = UserMaster::findOne(['phoneNumber'=>$phoneNumber,'status'=>'1']);
        
        if(!empty($user)){
            throw new \Exception(self::userExistPhone);
        }
    }
   
    /**
     * 
     * @param type $user
     * @param type $currentDateTime
     */
    public static function directLogin($user,$currentDateTime){
        $userId = $user->userId;
        $stepsCompleted=$user->signupSteps;
        SignUpAction::makeSession($userId, $currentDateTime,$newUser=0,$stepsCompleted);
    }
   
    /**
     * 
     * @throws \Exception
     */
    protected function insertUserData() {
        try {

            //self::checkPhoneNumberExists($this->get_data()->getPhoneNumber());
            self::checkEmailExists($this->get_data());
                       
            $userId = self::generateUniqueId();

            $user = new UserMaster();
            $user->userId = $userId;
           
            $user->fullName =  $this->get_data()->getFullName();
            $user->emailId = $this->get_data()->getEmailId();
            $user->password = $this->get_data()->getPassword();
            $user->phoneNumber = $this->get_data()->getPhoneNumber();
            $user->profilePic = "/images/user-profile.png";
           
            
            
            //$user->dob      = $this->get_data()->getDob();
            $user->gender   = $this->get_data()->getGender();
            $user->roleName = $this->get_data()->getRoleName();
            
            $user->createdDate = $this->_currDateTime;
            $user->updatedDate = $this->_currDateTime;
            
            $user->createdBy = $userId;
            $user->updatedBy = $userId;
            

            $user->save();
            $getuserId = $user->id; 

            if (empty($getuserId)) {
                throw new \Exception(self::userNotCreated);
            }

            $user = UserMaster::find()->where(['id'=>$user->id])->one();
            Validators::setUserObject($user);
            $stepsCompleted=$this->get_data()->getSignupSteps();
            
            
            if($this->flag==1){
                
                $fData= ['isNewUser'=>1,'signupSteps'=>(int)$stepsCompleted,
                    'fullName'=> Validators::getUserObject()->fullName,
                    'userId'=>(float)$userId,'refreshToken'=>null,'sessionToken'=>null,'message'=> $this->getSucessMessage(),'profileObject'=>null];
            
                $_response = ['response' => true, 'code' => Globals::SUCCESSCODE,'message'=>$msg,'data'=>$fData, 'type' => Validators::get_type(), 'errorMessage' => null];
                Globals::returnJsonResponse($_response);
                exit;
            }
            
            self::makeSession($userId, $this->_currDateTime,$newUser=1,$stepsCompleted);
            
        } catch (\Exception $ex) {
            
            new AppException(!empty($ex->errorInfo[1])?$ex->errorInfo[1]:NULL, Validators::get_type());
            $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' => Validators::get_type(), 'errorMessage' => $ex->getMessage());
            Globals::returnJsonResponse($_response);
            exit;
        }
    }
    
    /**
     * 
     * @param type $min
     * @param type $max
     * @return type
     */
    public static function generateUniqueId($min=4,$max=8){
        try{
            
            $_random      =rand($min,$max);
            
            $tempUserid   = Globals::generateRandomInt($_random);
            $user         = UserMaster::find()->where(['userId'=>$tempUserid])->count();
            if($user>0){
                self::generateUserId();
            }
            
            return $tempUserid;
            
        } catch (\Exception $ex) {
            $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' => Validators::get_type(), 'errorMessage' =>self::userNotCreated);
            Globals::returnJsonResponse($_response);
            exit;
        }
    }
    
    
    /**
     * 
     * @param type $userId
     * @param type $dateTime
     * @param type $newUser
     * @param type $stepsCompleted
     * @param type $msg
     * @throws \Exception
     */
    public static function makeSession($userId,$dateTime,$newUser=0,$stepsCompleted=0,$msg=self::successUserCreation){
        try{
            
            $getMyprofile = self::getMyProfile();
            
            $userSess                   = new UserSession();
            $userSess->userMasterId     = $userId;    
            $userSess->refreshToken     = Globals::generateSession(50);
            $userSess->sessionToken     = Globals::generateSession(100);
            $userSess->createdDate      = $dateTime;
            $userSess->updatedDate      = $dateTime;
            $userSess->expiryDate       = Globals::customDate(30,Validators::get_type()); // 30 days Sesssion
            
            $userSess->ipAddress        = Globals::getClientIPaddress();
            $userSess->browserInfo      = json_encode(Globals::getClientBrowserDetail());
            
            $userSess->save();
            
            $sessId=$userSess->id;
            if(empty($sessId)){
                throw new \Exception('Failed To Logged In User');
            }
           
            
            $fData= ['isNewUser'=>(int)$newUser,'signupSteps'=>(int)$stepsCompleted,
                    'fullName'=> Validators::getUserObject()->fullName,
                    'userId'=>(float)$userId,'refreshToken'=>$userSess->refreshToken,'sessionToken'=>$userSess->sessionToken,'message'=>$msg,'profileObject'=>$getMyprofile];
            
            $_response = ['response' => true, 'code' => Globals::SUCCESSCODE,'message'=>$msg,'data'=>$fData, 'type' => Validators::get_type(), 'errorMessage' => null];
            Globals::returnJsonResponse($_response);
            exit;
            
        } catch (\Exception $ex) {
            
           new AppException(!empty($ex->errorInfo[1])?$ex->errorInfo[1]:NULL, Validators::get_type());
            $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' => Validators::get_type(), 'errorMessage' => $ex->getMessage());
            Globals::returnJsonResponse($_response);
            exit;
        }
    }
    
    
    /**
     * 
     * @throws \Exception
     */
    
    public static function getMyProfile(){
        try{
             
            $userProfile = new UserProfileDAO();
            
            $userObject = Validators::getUserObject();
            $userProfile->setUserId($userObject->userId);
            $userProfile->setPhoneNumber($userObject->phoneNumber);
            $userProfile->setEmailId($userObject->emailId);
            $userProfile->setFullName($userObject->fullName);
            $userProfile->setProfilePic($userObject->profilePic);
            $userProfile->setSignupSteps($userObject->signupSteps);
            $userProfile->setRoleName($userObject->roleName);
            $userProfile->setGender($userObject->gender);
            $userProfile->setDob($userObject->dob);

            $fData= [
                    'profile'=>$userProfile
                    ];
            
            return $fData;
            
        } catch (\Exception $ex) {
            new AppException(!empty($ex->errorInfo[1])?$ex->errorInfo[1]:NULL, Validators::get_type());
            $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' => Validators::get_type(), 'errorMessage' => $ex->getMessage());
            Globals::returnJsonResponse($_response);
            exit;
        }
    }

        /**
     * 
     * @return HomelessDAO
     */
    public static function getHomelessProfile(){
        try{
            $userProfile = new HomelessDAO();
            $ma = new MADAO();
            
            $userObject = HomlessMaster::find()->where(['userId'=> Validators::getUserObject()->userId])->one();
            $userProfile->setUserId($userObject->userId);
            $userProfile->setPhoneNumber($userObject->phoneNumber);
            $userProfile->setEmailId($userObject->emailId);
            $userProfile->setFullName($userObject->fullName);
            $userProfile->setProfilePic($userObject->profilePic);
            $userProfile->setSignupSteps($userObject->signupSteps);
            $userProfile->setFbId($userObject->fbId);
            $userProfile->setGId($userObject->gId);
            $userProfile->setBio($userObject->bio);
            $userProfile->setCountryCode($userObject->countryCode);
            $userProfile->setGender($userObject->gender);
            $userProfile->setDob($userObject->dob);

            $userProfile->setAddress1($userObject->address1);
            $userProfile->setAddress2($userObject->address2);
            $userProfile->setZipCode($userObject->zipCode);
            $userProfile->setState($userObject->state);
            $userProfile->setCountry($userObject->country);
            $userProfile->setCity($userObject->city);
            $userProfile->setRoleName($userObject->roleName);
            $userProfile->setWallet($userObject->wallet);
            $userProfile->setWorksAt($userObject->worksAt);
            $userProfile->setStatus($userObject->status);
            $userProfile->setDoc($userObject->doc);

            $userProfile->setOrgId($userObject->orgId);
            $userProfile->setLat($userObject->lat);
            $userProfile->setLong($userObject->long);
            $userProfile->setDeviceid($userObject->deviceId);
            $userProfile->setIsdevice($userObject->isDevice);
            $userProfile->setAccount($ma);

            $fData= [
                    'profile'=>$userProfile
                    ];
            
            return $fData;
            
        } catch (\Exception $ex) {
            new AppException(!empty($ex->errorInfo[1])?$ex->errorInfo[1]:NULL, Validators::get_type());
            $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' => Validators::get_type(), 'errorMessage' => $ex->getMessage());
            Globals::returnJsonResponse($_response);
            exit;
        }
    }
    
    public function adminNotify($user) {
        $obj = new NotificationsDAO();

        $obj->setUserMasterId($user->userId);
        $obj->setNotificationType(Globals::USER_ADD);
        $obj->setUrl("/dashboard.php");
        $obj->setDescription("New <b>$user->roleName</b> with Name <b>`$user->fullName`</b> as  has been registered with ID : <b>$user->userId</b> at $this->_currDateTime");

        $obj->setCreatedBy($user->userId);
        $obj->setUpdatedBy($user->userId);
        $obj->setCreatedDate($this->_currDateTime);
        $obj->setUpdatedDate($this->_currDateTime);
        $obj->setTblId($user->id);

        Globals::notification($obj);
    }

}
