<?php

/**
 * @author Saurabh Chhhabra <saurabhchhabra018@gmail.com>
 * @link http://localhost:8181/site/getapi Get all API's in Listed Format
 * @
 */
/**
 * Master Name-spaces
 */

namespace app\controllers;

namespace app\controllers\v1\globalmod\login;

/** Inherit Name-spaces * */
use \yii\web\Controller as BaseController;
use app\components\GlobalController as Globals;
use app\components\AppException as AppException;

use app\components\v1\messages\Messages as Messages;

/* * * Validations & Validators Controllers * */
use app\components\APIValidations as APIValidations;
use app\components\ValidatorsController as Validators;

/* * * Modal Controllers * */
use app\models\UserMaster as UserMaster;

/** **/
use app\controllers\v1\globalmod\login\SignUpAction as SignUpAction;

final class LoginAction extends Messages {

    public $_currDateTime;
    
    private $_data;
    
    private $sucessMessage = self::successLogin;
     
    public function get_data() {
        return $this->_data;
    }

    public function set_data($_data) {
        $this->_data = $_data;
    }

    public function getSucessMessage() {
        return $this->sucessMessage;
    }

    public function setSucessMessage($sucessMessage) {
        $this->sucessMessage = $sucessMessage;
        return $this;
    }
    
    public function runWithParams() {

        try {
            $_type = "Login";
            $contentType = Globals::APPLICATIONJSON;

            Validators::set_type($_type);
            
            $global = new Globals;
            $this->_currDateTime = $global->DATETIME;

            /**
             * Check Pre-Flight Headers
             * 
             * @step 1
             */
            Globals::corsHeader();
            Globals::checkHeader($contentType, $_type);
            Globals::checkRequest('POST', $_type);

            /**
             * Check API Validations for Required Keys
             * 
             * @step 2
             */
            /* @var $data type for JOSN Data */
            $data = \json_decode(file_get_contents('php://input'), true);

            $required = array('emailId','password');
            /**
             * Check if Some Required Fields is Empty or Null
             * 
             * @step 3
             */
            $validate = APIValidations::checkMandatoryKeys($data, $required); //print_r($validate);die;

            if ($validate != 'ok') {
                $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' => Validators::get_type(), 'errorMessage' => APIValidations::$statusVar[$validate] . Globals::cannotBlank);
                Globals::returnJsonResponse($_response);
                exit;
            }

            /**
             * Check if Some Field is Not Defined Push into Final Array & make as NULL
             * 
             * @step 4
             */
            $jsonArray = ['emailId','password'];
            $data = Validators::checkIsNullExp($data, $jsonArray, Validators::get_type());
            
            $this->set_data($data);
            
            Validators::stringType($this->get_data()['emailId'], Globals::STRING_DATA_LEN_255, Validators::get_type());
            Validators::stringType($this->get_data()['password'], Globals::STRING_DATA_LEN_255, Validators::get_type());
            
            /**
             * Play with Data
             * 
             * @step 5
             */
            $this->loginData();
        } catch (\Exception $ex) {
           new AppException(!empty($ex->errorInfo[1])?$ex->errorInfo[1]:NULL, Validators::get_type());
            $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' => Validators::get_type(), 'errorMessage' => $ex->getMessage());
            Globals::returnJsonResponse($_response);
            exit;
        }
    }

    /**
     * 
     * @throws \Exception
     */
    protected function loginData() {
        try {
            
            $password = Globals::md5Pass($this->get_data()['password']);
            
            $user = UserMaster::findOne(["emailId" =>  $this->get_data()['emailId'],"status"=>"1"]);
            if (empty($user)) {
                throw new \Exception(self::userNotFound .$this->get_data()['emailId']);              
            }

            if($user->password == $password){
               $msg = self::userLoggedIn;
               Validators::setUserObject($user);
               SignUpAction::makeSession($user->userId, $this->_currDateTime,$newUser=0,$user->signupSteps,$msg); 
            }else{
                throw new \Exception(self::wrongUserName);
            }

            
        } catch (\Exception $ex) {
           new AppException(!empty($ex->errorInfo[1])?$ex->errorInfo[1]:NULL, Validators::get_type());
            $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' => Validators::get_type(), 'errorMessage' => $ex->getMessage());
            Globals::returnJsonResponse($_response);
            exit;
        }
    }

}
