<?php

/**
 * @author Saurabh Chhhabra <saurabhchhabra018@gmail.com>
 * @link http://localhost:8181/site/getapi Get all API's in Listed Format
 * @
 */


/**
 * Master Name-spaces
 */
namespace app\controllers;
namespace app\controllers\v1;

/** Inherit Name-spaces **/
use \yii\web\Controller as BaseController;
use app\components\GlobalController as Globals;
use app\components\AppException as AppException;
use app\components\v1\messages\Messages as Messages;

/*** Validations & Validators Controllers **/
use app\components\APIValidations as APIValidations;
use app\components\ValidatorsController as Validators;

/*** Modal Controllers **/
use app\models\UserMaster as UserMaster;
use app\models\UserSession as UserSession;

use app\controllers\v1\CheckInviteCodeAction as CheckInviteCode;

final class LogoutAction extends Messages {
    
    public $_currDateTime;
    public $_userMasterId;
    private $sessionToken;
    
    private $_data;
    
    private $sucessMessage = self::successLogout;
    private $_errorMessage  = self::errorLogout;
     
    public function get_data() {
        return $this->_data;
    }

    public function set_data($_data) {
        $this->_data = $_data;
    }

    public function getSucessMessage() {
        return $this->sucessMessage;
    }

    public function setSucessMessage($sucessMessage) {
        $this->sucessMessage = $sucessMessage;
        return $this;
    }

    
    public function runWithParams()
    {
        
        try{
            $_type = "Logout";
            $contentType=Globals::APPLICATIONJSON;
            
            $global=new Globals;    
            Validators::set_type($_type);

            $global = new Globals;
            $this->_currDateTime = $global->DATETIME;
            
            /**
             * Check Pre-Flight Headers
             * 
             * @step 1
             */
            Globals::corsHeader();
            Globals::checkHeader($contentType,Validators::get_type());
            Globals::checkRequest('PUT',Validators::get_type());
            $this->_userMasterId=Validators::checkUserSession(Validators::get_type());
            $this->sessionToken = $_SERVER["HTTP_SESSIONTOKEN"];
            /**
             * Check API Validations for Required Keys
             * 
             * @step 2
             */
            
            /* @var $data type for JOSN Data */
            
            
            /**
             * Check if Some Field is Not Defined Push into Final Array & make as NULL
             * 
             * @step 4
             */
            
            

            /**
             * Play with Data
             * 
             * @step 5
             */
            $this->logout();
            
            
        } catch (\Exception $ex) {
          new AppException(!empty($ex->errorInfo[1])?$ex->errorInfo[1]:NULL, Validators::get_type());
            $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' => Validators::get_type(), 'errorMessage' => $ex->getMessage());
            Globals::returnJsonResponse($_response);
            exit;
        }
        
       
    }
    
    
    /**
     * 
     */
    protected function logout(){
        try{
            
            $msg=self::successLogout;
            
            $userSess                   = UserSession::findOne(['sessionToken'=> $this->sessionToken,'userMasterId'=> $this->_userMasterId]);
            $userSess->status     = 0;    
            $userSess->save();
           
            $fData= ['message'=>$msg];
            
            $_response = ['response' => true, 'code' => Globals::SUCCESSCODE,'message'=>$msg,'data'=>$fData, 'type' => Validators::get_type(), 'errorMessage' => null];
            Globals::returnJsonResponse($_response);
            exit;
            
        } catch (\Exception $ex) {
           new AppException(!empty($ex->errorInfo[1])?$ex->errorInfo[1]:NULL, Validators::get_type());
            $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' => Validators::get_type(), 'errorMessage' => $ex->getMessage());
            Globals::returnJsonResponse($_response);
            exit;
        }
    }
}
