<?php

/**
 * @author Saurabh Chhhabra <saurabhchhabra018@gmail.com>
 * @link http://localhost:8181/site/getapi Get all API's in Listed Format
 * @routername checkInviteCode
 */
/**
 * Master Name-spaces
 */

namespace app\controllers;

namespace app\controllers\v1\user;

/** Inherit Name-spaces * */
use \yii\web\Controller as BaseController;
use app\components\GlobalController as Globals;
use app\components\AppException as AppException;
use app\components\v1\messages\Messages;

/* * * Validations & Validators Controllers * */
use app\components\APIValidations as APIValidations;
use app\components\ValidatorsController as Validators;


/* * * Modal Controllers * */
use app\models\view\VwExpense;

/** PAGINATION * */
use app\models\modeloject\ModelClass;

final class GetAllExpensesAction extends Messages {

    public $_currDateTime;
    public $_userMasterId;
    private $sucessMessage = self::allUsers;
    private $_errorMessage = self::allUsersErr;
    
    public $stylistId = 0;

    public function getSucessMessage() {
        return $this->sucessMessage;
    }

    public function setSucessMessage($sucessMessage) {
        $this->sucessMessage = $sucessMessage;
        return $this;
    }

    public function getStylistId() {
        return $this->stylistId;
    }

    public function setStylistId($stylistId) {
        $this->stylistId = $stylistId;
        return $this;
    }

    
    public function runWithParams($param) {

        try {
            $_type = "GetAllExpenses";
            $contentType = Globals::APPLICATIONJSON;

            Validators::set_type($_type);

            $global = new Globals;
            $this->_currDateTime = $global->DATETIME;

            /**
             * Check Pre-Flight Headers
             * 
             * @step 1
             */
            Globals::corsHeader();
            Globals::checkHeader($contentType, $_type);
            Globals::checkRequest('GET', Validators::get_type());
            $this->_userMasterId = Validators::checkUserSession(Validators::get_type(),Globals::USER);

            /**
             * Check API Validations for Required Keys
             * 
             * @step 2
             */
            /* @var $data type for JOSN Data */


            /**
             * Check if Some Field is Not Defined Push into Final Array & make as NULL
             * 
             * @step 4
             */
            /**
             * Play with Data
             * 
             * @step 5
             */
            $this->getAllExpenses();
        } catch (\Exception $ex) {
            new AppException(!empty($ex->errorInfo[1]) ? $ex->errorInfo[1] : NULL, Validators::get_type());
            $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' => Validators::get_type(), 'errorMessage' => $ex->getMessage());
            Globals::returnJsonResponse($_response);
            exit;
        }
    }

    /**
     * 
     */
    protected function getAllExpenses() {
        try {
            
            $model   = new ModelClass();
            $alluser = VwExpense::find()->where(['user_id'=> $this->_userMasterId])->asArray()->all();
          
            
            $fData = ['message' => $this->getSucessMessage(), 'list' => $alluser];

            $_response = ['response' => true, 'code' => Globals::SUCCESSCODE, 'message' => $this->getSucessMessage(), 'data' => $fData, 'type' => Validators::get_type(), 'errorMessage' => null];
            Globals::returnJsonResponse($_response);
            exit;
        } catch (\Exception $ex) {
            new AppException(!empty($ex->errorInfo[1]) ? $ex->errorInfo[1] : NULL, Validators::get_type());
            $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' => Validators::get_type(), 'errorMessage' => $ex->getMessage());
            Globals::returnJsonResponse($_response);
            exit;
        }
    }

}
