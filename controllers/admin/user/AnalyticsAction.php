<?php

/**
 * @author Saurabh Chhhabra <saurabhchhabra018@gmail.com>
 * @link http://localhost:8181/site/getapi Get all API's in Listed Format
 * @routername checkInviteCode
 */
/**
 * Master Name-spaces
 */

namespace app\controllers;

namespace app\controllers\admin\user;

/** Inherit Name-spaces * */
use \yii\web\Controller as BaseController;
use app\components\GlobalController as Globals;
use app\components\AppException as AppException;
use app\components\v1\messages\Messages;

/* * * Validations & Validators Controllers * */
use app\components\APIValidations as APIValidations;
use app\components\ValidatorsController as Validators;


/* * * Modal Controllers * */
use app\models\DailyuserExpense;
use app\models\MonthlyuserExpense;

use app\models\view\VwAllExpense;
use app\models\view\VwMonthlyExpense;
use app\models\view\VwYearlyExpense;
use app\models\view\Vwcatdata;;
/** PAGINATION * */


final class AnalyticsAction extends Messages {

    public $_currDateTime;
    public $_userMasterId;
    private $sucessMessage = self::config;
    private $_errorMessage = self::config;
    
    public $stylistId = 0;

    public function getSucessMessage() {
        return $this->sucessMessage;
    }

    public function setSucessMessage($sucessMessage) {
        $this->sucessMessage = $sucessMessage;
        return $this;
    }


    
    public function runWithParams($param) {

        try {
            $_type = "Config";
            $contentType = Globals::APPLICATIONJSON;

            Validators::set_type($_type);

            $global = new Globals;
            $this->_currDateTime = $global->DATETIME;

            /**
             * Check Pre-Flight Headers
             * 
             * @step 1
             */
            Globals::corsHeader();
            Globals::checkHeader($contentType, $_type);
            Globals::checkRequest('GET', Validators::get_type());
            $this->_userMasterId = Validators::checkUserSession(Validators::get_type(),  Globals::ADMIN);

            /**
             * Check API Validations for Required Keys
             * 
             * @step 2
             */
            /* @var $data type for JOSN Data */


            /**
             * Check if Some Field is Not Defined Push into Final Array & make as NULL
             * 
             * @step 4
             */
            /**
             * Play with Data
             * 
             * @step 5
             */
            $this->config();
        } catch (\Exception $ex) {
            new AppException(!empty($ex->errorInfo[1]) ? $ex->errorInfo[1] : NULL, Validators::get_type());
            $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' => Validators::get_type(), 'errorMessage' => $ex->getMessage());
            Globals::returnJsonResponse($_response);
            exit;
        }
    }

    /**
     * 
     */
    protected function config() {
        try {
              
            $year = isset($_REQUEST['year'])?$_REQUEST['year']:"0";
            $month = isset($_REQUEST['month'])?$_REQUEST['month']:"0";
           
           
            $analytics = new \stdClass();
            $analytics = new \stdClass();
            $analytics->allexpense = DailyuserExpense::find()->select('sum(total) as total,category,date')->where(['year(date)'=>$year,'month(date)'=>$month])
                    ->asArray()->groupBy('date')->orderBy('date asc')->all();
            if($year=="0"){
                $analytics->allexpense = MonthlyuserExpense::find()->select('sum(total) as total,category,date')->where(['month(date)'=>$month])->asArray()->groupBy('date')->orderBy('date asc')->all();
            }
            if($month=="0"){
                $analytics->allexpense = MonthlyuserExpense::find()->select('sum(total) as total,category,date')->where(['year'=>$year])->asArray()->groupBy('date')->orderBy('date asc')->all();
            }
            
            
            $allexpense = VwAllExpense::find()->select('sum(totalexpense) as totalexpense')->where([])->asArray()->one();
            $allexpense2 = VwMonthlyExpense::find()->select('sum(totalexpense) as totalexpense')->where([])->asArray()->one();
            $allexpense3 = VwYearlyExpense::find()->select('sum(totalexpense) as totalexpense')->where([])->asArray()->one();
            //$cat = Vwcatdata::find()->select('x,sum(y) as y')->where(['year'=>$year,'month'=>$month])->groupBy('category')->asArray()->all();
            
            if($month==0){
                $cat = Vwcatdata::find()->select('x,y')->where(['year'=>$year])->groupBy('x')->asArray()->all();
            }else{
                $cat = Vwcatdata::find()->select('x,y')->where(['year'=>$year,'month'=>$month])->groupBy('x')->asArray()->all();
            }
            
            //print_r($analytics);die;
            
            $fData = ['message' => $this->getSucessMessage(), 'list' => $analytics,'expense'=>['all'=>$allexpense,'m'=>$allexpense2,'y'=>$allexpense3,'cat'=>$cat]];

            $_response = ['response' => true, 'code' => Globals::SUCCESSCODE, 'message' => $this->getSucessMessage(), 'data' => $fData, 'type' => Validators::get_type(), 'errorMessage' => null];
            Globals::returnJsonResponse($_response);
            exit;
        } catch (\Exception $ex) {
            new AppException(!empty($ex->errorInfo[1]) ? $ex->errorInfo[1] : NULL, Validators::get_type());
            $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' => Validators::get_type(), 'errorMessage' => $ex->getMessage());
            Globals::returnJsonResponse($_response);
            exit;
        }
    }

}
