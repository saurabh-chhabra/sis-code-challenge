<?php

/**
 * @Author : Saurabh Chhabra <saurabhchhabra018@gmail.com>
 * @Description : API Validations
 */

namespace app\components;

class APIValidations {

    
    public static $statusVar =array(
        "emailId"           =>  "Email Id",
        "fbId"              =>  "FaceBook Id",
        "gId"               =>  "Google Id",
        "name"              =>  "Full Name",     
        "password"          =>  "Password",
        "message"           =>  "Message",
        "dob"               =>  "Date of Birth",
        "roleName"          =>  "Registered as",
        "countryCode"       =>  "Country Code",
        "gender"            =>  "Gender",
        "confirmPassword"   =>  "Confirm Password",
        "oldPassword"       =>  "Old Password",
        "sessionToken"      =>  "Session Token",
        "phoneNumber"       =>  "PhoneNumber",
        "createdBy"         =>  "Created By",
        "updatedBy"         =>  "Updated By",
        "createdDate"       =>  "Created Date",
        "updatedDate"       =>  "Updated Date",
        "status"            =>  "status",
        "userMasterId"      =>  "User Id",
        "ipAddress"         =>  "Ip Address",
        "browserInfo"       =>  "Browser Info",
        "inviteCode"        =>	"Invite Code",
        "discount"          =>  "Discount",
        "lat"               =>  "Latitude",
        "lng"               =>  "Longitude",
        "city"              =>  "City",
        "searchTextField"   =>  "Search Text Field",
        "homelessId"        =>  "Homeless Id",
        "description"       =>  "Description",
        "title"             =>  "Title",
        "location"          =>  "Location",
        "pin"               =>  "Pin",
        "FILE_IMG"          =>  [
            "type"    => "File Type",
            "name"    => "Image Name",
            "tmp_name"=> "File Type"
        ],
        "OTPVerify"         => [
            "param1"    =>  "Phone Number",
            "param2"    =>  "Otp"
        ],
        "GetStylistBySalon"  => [
            "param1"    =>  "Salon Id",
        ],
        "GetServicesBySalon" => [
            "param1"    =>  "Salon Id",
        ],
        "DeleteUser" =>[
            "param1"    =>  "Customer Id"
        ],
        "AddCoupon"=>[
            "coupon" => "Coupon Name",
            "discountPrice" => "Discount Price",
            "displayPrice" => "Discount Display Price",
            "expiryDate" => "Expiry Date",
        ],
        "DeleteCoupon"=>[
            "param1" => "Coupon Id"
        ],        
    );



    /**
     * 
     * @param type $request_array
     * @param type $required
     * @return strings
     */
    public static function checkMandatoryKeys($request_array = '', $required = array()) {
		
        foreach ($required as $key) {
            if (!isset($request_array[$key]) || $request_array[$key] == '') {
                return $key;
            }
        }

        return "ok";
    }

    /**
     * 
     * @param type $name
     * @param type $type
     * @throws Exception
     */
    public static function validateName($name, $type) {

        if (!preg_match("/^[a-zA-Z ]*$/", $name)) {
            throw new \Exception("Only letters and white space allowed");
        }
    }

    /**
     * 
     * @param type $number
     * @param type $type
     * @throws Exception
     */
    public static function validateNumber($number, $type) {

        if (!preg_match('/^[0-9]{10}+$/', $number)) {
            throw new \Exception("Only 10 Numbers allowed");
        }
    }

    /**
     * 
     * @param type $email
     * @param type $type
     * @throws Exception
     */
    public static function validateEmail($email, $type) {

        self::validateDns($email);

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception("Not a Valid Email");
        }
    }

    /**
     * 
     * @param type $email
     * @throws Exception
     */
    public static function validateDns($email) {
        $domain = substr($email, strpos($email, '@') + 1);
        if(empty($domain)){
            throw new \Exception("Email Domain is not Valid");
        }
        if (checkdnsrr($domain) == FALSE) {
            throw new \Exception("Email Domain is not Valid");
        }
    }

    /**
     * 
     * @param type $dob
     * @param type $type
     * @throws Exception
     */
    public static function validateDob($dob, $type) {
        list($yyyy, $mm, $dd) = explode('-', $dob);
        if (!checkdate($mm, $dd, $yyyy)) {
            throw new \Exception("Not a Valid DOB . Must be in YYYY-MM-DD Format");
        }
    }

    /**
     * 
     * @param type $gender
     * @param type $type
     * @return type
     * @throws Exception
     */
    public static function validateGender($gender, $type) {
        if (strtoupper($gender) == 'MALE' || strtoupper($gender) == 'FEMALE')
            return;
        else
            throw new \Exception('Not a Valid Gender M/F');
    }
    
    /**
     * 
     * @param type $data
     * @param type $array
     * @return boolean
     * @throws \Exception
     */
    public static function checkInArray($data,$array=array()){
        if(in_array($data,$array)){
            return true;
        }else{
            throw new \Exception('Wrong Value Passed');
        }
    }

}
