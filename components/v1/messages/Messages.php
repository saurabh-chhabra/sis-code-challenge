<?php

/**
 * Description of SuccessMessages
 *
 * @author Saurabh Chhabra <saurabhchhabra018@gmail.com>
 */

namespace app\components\v1\messages;

use \yii\web\Controller as BaseController;
use \app\components\GlobalController as Globals;

class Messages  extends BaseController{
    //put your code here
    
    /**
     * @inheritdoc
     * @return static|null ActiveRecord instance matching the condition, or `null` if nothing matches.
     */
    
    const en                    = 'en';
    const hi                    = 'hi';
    const fb                    = "fb";
    const contactUs             = "Our Representative will get back to you shortly";
    
    const errorLanguage         = "Language Not Found";
    const errorDeveloper        = "Please contact admin";
    
    const successUserCreation   = "User has been Successfully Registered. Please Login";
    const homelessRegister      = "Homeless has been Successfully Registered";
    const homelessUpdate        = "Homeless has been Successfully Updated";
    const mercRegister          = "Merchant has been Successfully Registered. Profile will be activated when Admin approves";
    const orgRegister           = "Organisation has been Successfully Registered. Profile will be activated when Admin approves";
          
    const successLogin          = "You have Sucessfully logged";
    const errorLogin            = "Failed to logged in";
    const wrongUserName         = "Your UserName or Password is incorrect";
    const userLoggedIn          = "User Successfully Logged In";
    const userNotFound          = "User Not Found With User Name :";
    const invalidEmail          = "User is not registered with EmailId";
    const invalidPhone          = "User is not registered with Phone-Number";
    const socialUser            = "Password cannot be updated for Facebook/Google user";
    const userCreated           = "User Successfully Created";
    const userExistEmailId      = "User Already Exist with Email Id";
    const userExistPhone        = "User Already Exist with Phone Number";
    const userNotCreated        = "Failed To Create User";
    const userIdNotCreated      = "User Id is not Created";
    const generateOtp           = "Please Generate Otp";
    const invalidOtp            = "Invalid Otp";
    const validOtp              = "Valid Otp";
    
    const successSignUp         = "User Successfully Created";
    const errorSignUp           = "Failed to SignUp User";
    
    const successLogout         = "User Successfully Logout";
    const errorLogout           = "Failed to logout User";
    
    const successSkipSteps      = "Successfully skip steps";
    const errorSkipSteps        = "Failed to update skip steps";
    
    const mailSent              = "Successfully updated password and sent sms";
    const mailFail              = "Failed to update password";

    const errorBothPassword     = "Password and Confirm Password Does Not Match";
    const errorCurrentPassword  = "Your Current Password is incorrect";
    
    const successMyProfile      = "Successfully get Your Profile";
    const errorMyProfile        = "Failed to get Your Profile";
    
    const successProfileUpdate  = "Successfully updated Profile";
    const errorProfileUpdate    = "Failed to update Profile";
  
    const notification          = "Successfully get Notification List";
    const notificationErr       = "Failed to get Notification List";
    const notificationRead      = "Successfully mark all Notification as Read";
    const notificationReadErr   = "Failed to update Notification";
    
    const invalidCurrency       = "Invalid Currency";

    const setCoupon             = "Successfully add new coupon";
    const setCouponErr          = "Failed to add new coupon";
    
    const delCoupon             = "Successfully deleted coupon";
    const delCouponErr          = "Failed to deleted coupon";

    const couponInfo            = "Successfully send coupon info to users";
    const couponInfoErr         = "Failed to send coupon info to users";

    const locationUpdate        = "Successfully updated Location";
    const locationUpdateErr     = "Failed to update Location";
    
    const walletPay             = "Successfully added Wallet Amount";
    const walletPayErr          = "Failed to add Wallet Amount";
    
    const walletTransfer        = "Successfully transfer funds";
    const walletTransferErr     = "Failed to transfer funds";
    const insuffiecientFunds    = "In-Sufficient Funds";
    const homelessNotFound      = "Homeless not found";
    const notValidMerchant      = "User is not a Business Owner";
    const invalidPin            = "Pin is Invalid";


    const donateToHomeless      = "Your Donation has been made sucessfully";
    const donateToHomelessErr   = "Failed to donated to homeless";
    
    const getWalletPay          = "Successfully get Wallet Txn";
    const getWalletPayErr       = "Failed to add Wallet Txn";
    
    const loc                   = "Successfully get Near By";
    const locErr                = "Failed to get Near By";
    
    const createJob             = "Job post created & is under review for public view";
    const createJobEr           = "Failed to job post";
    const updateJob             = "Job post updated & is under review for public view";
    const updateJobEr           = "Failed to update job post";
    const deleteJob             = "Job post is deleted";
    const deleteJobEr           = "Failed to delete job post";
    const invalidJob            = "Invalid Job Post";
    const getJob                = "Successfully get Job List";
    const getJobErr             = "Failed to get Job List";
    const approveJob            = "Job is Approved";
    
    const createMsg             = "Message Sent";
    const createMsgErr          = "Failed to sent message";
    const GetAllMsg             = "Successfully get Message Overview";
    const GetMyMsg              = "Successfully get My Conversation";
    
    
    const createAdv             = "Advertisement created & is under review for public view";
    const createAdvErr          = "Failed to create advertisement";
    const updateAdv             = "Advertisement updated & is under review for public view";
    const updateAdvEr           = "Failed to update Advertisement";
    const deleteAdv             = "Advertisement is deleted";
    const deleteAdvEr           = "Failed to delete Advertisement";
    const invalidAdv            = "Invalid Advertisement";
    const getAds                = "Successfully get Ads List";
    const getAdsErr             = "Failed to get Ads List";
    const approveAdv            = "Advertisement is Approved";
    
    const pinGenerate           = "Successfully generate pin";
    const pinGenerateErr        = "Failed to generate pin";
    
    const allUsers              = "Successfully get All Users";
    const allUsersErr           = "Failed to get All Users";
    
    const delUser              = "Successfully Delete User";
    const delUserErr           = "Failed to Delete User";
    
    const allData               = "Successfully get All Data";
    const allDataErr            = "Failed to get All Data";
    
    const actUser               = "Successfully Re-Enabled User";
    const actUserErr            = "Failed to Re-Enabled User";
    
    const delBlog               = "Successfully Delete Blog";
    const delBlogErr            = "Failed to Delete Blog";
    
    const updateBlog            = "Successfully Update Blog";
    const updateBlogErr         = "Failed to Update Blog";
    
    const addBlog               = "Successfully add new Blog";
    const addBlogErr            = "Failed to add new Blog";
    
    const getBlogs              = "Successfully get Blog list";
    const getBlogsErr           = "Failed to get Blog list";
    
    const setBlogs              = "Successfully saved new Blog";
    const setBlogsErr           = "Failed to save new Blog";
    
    const config                = "System Config";
    
    const addDevice             = "Device created";
    const addDeviceErr          = "Failed to create Device";
    const updateDevice          = "Device updated & is under review for public view";
    const updateDeviceEr        = "Failed to update Device";
    const deleteDevice          = "Device is deleted";
    const deleteDeviceEr        = "Failed to delete Device";
    const invalidDevice         = "Invalid Device";
    const getDevice             = "Successfully get Device List";
    const getDeviceErr          = "Failed to get Device List";


    public function construct() {
        
        $lang = isset($_SERVER["HTTP_ACCEPT_LANGUAGE"]) ? $_SERVER["HTTP_ACCEPT_LANGUAGE"] : 'en';
        
        switch ($lang){
            case self::en :
                
                break;
            case self::hi :
                break;
            
            default :
                //Globals::throwException(self::errorLanguage);
                
              
        }
    }
   
}
