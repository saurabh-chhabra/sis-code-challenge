<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Messages_HI
 *
 * @author Apparrant07
 */

namespace app\components\v1\messages\languages;

class Messages_hi {
    //put your code here
    
        
    const successLogin          = "You have Sucessfully logged";
    const errorLogin            = "You have Sucessfully logged";
    const wrongUserName         = "You have Sucessfully logged";
    const userLoggedIn          = "User Successfully Logged In";
    const userNotFound          = "User Not Found With User Names :";
    
    const successSignUp         = "User Successfully Created";
    const errorSignUp           = "Failed to SignUp User";
    
    const successLogout         = "User Successfully Logout";
    const errorLogout           = "Failed to logout User";
    
    const successSkipSteps      = "Successfully skip steps";
    const errorSkipSteps        = "Failed to update skip steps";
    
    const successPartnerLink    = "Partner is Successfuly Linked";
    const errorPartnerLink      = "Invite Code is not Valid";
    
    const alreadyPartner        = "Partner Already Linked";
    
    const mailSent              = "Successfully sent mail";
    const mailFail              = "Failed to send email";
    const errorBothPassword     = "Password and Confirm Password Does Not Match";
    const errorCurrentPassword  = "Your Current Password is incorrect";
    
    const successMyProfile      = "Successfully get Your Profile";
    const errorMyProfile        = "Failed to get Your Profile";
    
    const successPinLogin       = "Successfully Login via Pass Code";
    const errorrPinLogin        = "Failed to check Pass Code";
    const errorPassCode         = "Your Pass Code is Incorrect";
    
    const successProfileUpdate  = "Successfully updated Profile";
    const errorProfileUpdate    = "Failed to update Profile";
    
    const invalidCurrency       = "Invalid Currency";
    
    const successPaymentCat     = "Successfully Get Payment Categories";
    const errorPaymentCat       = "Failed to Get Payment Categories";
    const successCreatePayCat   = "Successfully Create Payment Category";
    const errorCreatePayCat     = "Failed to Create Payment Category";
    const alreadyPaymentExist   = "Payment Category Already Exist";
    const maxPayment            = "Payment Category(s) allowed";
    
    const successIncomeCat      = "Successfully Get Income Categories";
    const errorIncomeCat        = "Failed to Get Income Categories";
    const successCreateInCat    = "Successfully Create Income Category";
    const errorCreateInCat      = "Failed to Create Income Category";
    const alreadyIncomeExist    = "Income Category Already Exist";
    const maxIncome             = "Income Category(s) allowed";
    
    const successExpenseCat     = "Successfully Get Expense Categories";
    const errorExpenseCat       = "Failed to Get Expense Categories";   
    const successCreateExCat    = "Successfully Create Expense Category";
    const errorCreateExCat      = "Failed to Create Expense Category";
    const alreadyExpenseExist   = "Expense Category Already Exist";
    const maxExpense            = "Expense Category(s) allowed";
    
    const successAcivity        = "Successfully Get Activities";
    const errorAcivity          = "Failed to Get Activities";
    const successCreateAcivity  = "Successfully Created Activity";
    const errorCreateAcivity    = "Failed to Created Activity";
    const successDeleteAcivity  = "Successfully Deleted Activity";
    const errorDeleteAcivity    = "Failed to Delete Activity";
    const successUpdateAcivity  = "Successfully Updated Activity";
    const errorUpdateAcivity    = "Failed to Update Activity";
    const activityNotFound      = "Activity Not Found";
    
    
    public function __construct() {

    }
    
    public function callMe(){
        
    }
    
    public function notcalled() {
        try {
            $language = filter_input(INPUT_SERVER, 'HTTP_ACCEPT_LANGUAGE');
            if (!empty($language)) {
                $namepace = '\app\components\v1\messages\languages\\';
                $class = 'Messages_' . $language;
                $classFile = ".".DIRECTORY_SEPARATOR."languages".DIRECTORY_SEPARATOR.$class;
                if (!is_file($classFile)){
                   $class = $namepace."Messages_en";
                   $message = $class::getInstance();
                }else{
                    $class = $namepace.$class;
                    $message = $class::getInstance();
                }
               
            }   
            $reflectionClass = new \ReflectionClass($class);
            $this->setMessagesObj($reflectionClass);
            $id = $this->getMessagesObj()->getConstant('activityNotFound');            
        } catch (\Exception $ex) {
            
        }
    }
}

