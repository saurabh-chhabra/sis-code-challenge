<?php

/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * @Author : Saurabh Chhabra
 * @Description : Global Exception Used Anywhere
 * 
 * 
 */

namespace app\components;


// Base & Global Controllers
use \yii\web\Controller as BaseController;
use app\components\GlobalController as Globals;

// Models
use app\models\SQlExceptions as SQlExceptions;

class AppException extends BaseController {
    
    protected static $message;
    
    protected static $sqlId;
    
    public static $apiType = "ExceptionClass";

    
    /**
     * 
     * @param type $sqlId
     * @param type $type
     */
    public function __construct($sqlId,$type) {
        self::$sqlId    = $sqlId;
        self::$apiType  = $type;
        
        self::getExceptionState();
    }
    
    /**
     * 
     * @return type
     * @throws Exception
     */
    protected static function getExceptionState(){
        try{
            
            $exception = SQlExceptions::findOne(['sqlCode'=>self::$sqlId]); 
            
            if(empty($exception)){
                return;
            }
            
            throw new \Exception(trim($exception->exceptionMsg));
            
        } catch (\Exception $ex) {
             
            $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' => self::$apiType, 'errorMessage' => $ex->getMessage());
            Globals::returnJsonResponse($_response);
            exit;
        }
    }
    
    
}
