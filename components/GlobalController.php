<?php

/**
 * @Author : Saurabh Chhabra <saurabhchhabra018@gmail.com>
 * @Description : Global Functions Used Anywhere
 *  
 */

namespace app\components;

use app\components\AppException as AppException;
use app\models\dao\NotificationsDAO;
use app\models\Notifications;

/* * * Model Controllers * */
use app\models\Logs as Logs;

Class GlobalController extends AppException {

    // log id
    public static $LOG_ID;
    public static $APP_NAME = "Expense APP";

    const APPLICATIONJSON = "application/json";
    const cannotBlank = " can not be blank";
    const MAXIMUM = "Maximum ";
    const MINIMUM = "Minimum ";
    // Image Maz Size with SLUG ~ 2.3 MB
    const IMAGE_MAX_SIZE = '2300000';
    // HTTP CODES
    const SUCCESSCODE = 200;
    const REQUESTERRORCODE = 400;
    const ERRORCODE = 403;
    const METHODERRORCODE = 405;
    const LENGTHREQUIRED = 411;
    const PAYLOADTOOLARGE = 413;
    const UNSUPPORTEDTYPECODE = 415;
    const TOOMANYREQUESTERROR = 429;
    const USERSESSIONEXPIRED = 440;
    // GET POST PUT DELETE

    const GET = 'GET';
    const POST = 'POST';
    const PUT = 'PUT';
    const DELETE = 'DELETE';
    public static $HTTP_CODES = [200 => "OK", 403 => "Something Went Wrong",
        400 => "Request Not Found", 405 => "Bad Request",
        411 => "Param Length Not Matched", 413 => "Request Body Too Large",
        415 => "Content Type Not Found", 429 => "Too Many Request",
        440 => "Please Login Again"];
    // pagination limit global
    const sqlLIMIT = 50;
    

    // ROLES
    /*************************************/
    const USER = "USER";
    const ADMIN = "ADMIN";
    
    /*************************************/
    
    const PENDING = "Pending";
    const COMPLETED = "Completed";
    const REJECT_USER = "RejectedByUser";
    const REJECTED = "Rejected";
    const ACCEPTED = "Accepted";
    const PAID = "Paid";
    
    //ENUM('APT_REJECT', 'SHOP', 'APT_ACCEPT', 'APT_USR_REJ', 'OT', 'APT_ADDED')
    const USER_ADD          = "USER_ADD";
    
    const NOT_OT            = "OT";
    
    const NO_PARTNER = -1;
    // mail layout
    public static $MAIL_LAYOUT = array('layouts/html', 'layouts/forgot', 'layouts/reset');
    
    // data types
    const INTEGER_DATA_LEN_10 = 10;
    const INTEGER_DATA_LEN_11 = 11;
    const INTEGER_DATA_LEN_12 = 12;
    const INTEGER_DATA_LEN_20 = 20;
    const STRING_DATA_LEN_4 = 4;
    const STRING_DATA_LEN_6 = 6;
    const STRING_DATA_LEN_100 = 100;
    const STRING_DATA_LEN_120 = 120;
    const STRING_DATA_LEN_200 = 200;
    const STRING_DATA_LEN_255 = 255;
    const STRING_DATA_LEN_500 = 500;
    const LONG_TXT            = 10000000;
    const ACTIVE = 1;
    const INACTIVE = 0;

    public $DATETIME;
    public static $DATE_TIME;
    // gender

    public static $MALE = 'Male';
    public static $FEMALE = 'Female';
    // tagged privacy
    public static $taggingPrivacy = [
        'ALL', 'MY_CONNECTIONS'
    ];
    public static $sqlCount = 'Select count(*) as totalCount ';
    public static $GETIMGAEURL = "";
    public static $IMGAEURL = "";
    public static $MEDIAURL = "";
    public static $ICONDIR = "icons/";
    public static $PNGEXTENSION = ".png";
    // write us max images for saving
    public static $imageMax = 4;
    public static $colorArray = ['#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6',
        '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
        '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A',
        '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
        '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC',
        '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
        '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680',
        '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
        '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3',
        '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'];
    public static $MONTH_NAME = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
    public static $pushPurpose = ['ACTIVITY', 'COMMENTS'];
    public static $notificationTitle = [
        "ACTIVITY" => "ACTIVITY"
    ];

    const GOOGLE_KEY = 'AIzaSyCFVPLYcp_DJaDVgBNZ8C3-21qgUgFae7Q';
    const TWILLIO_KEY = 'E2Va6vLZhB6ity0byq5v0xecajcLJgsB';

    /**
     *  __construct
     */
    public function __construct() {
        date_default_timezone_set('Asia/Kolkata');
        $this->DATETIME = date("Y-m-d H:i:s");
        self::$DATE_TIME = date("Y-m-d H:i:s");
        self::corsHeader();
    }

    /**
     * 
     * @param type $sql
     * @return type
     */
    public static function sqlCount($sql = '*') {
        $sql = "Select count($sql) as totalCount ";

        return $sql;
    }

    /**
     * 
     * @param type $password
     * @return type
     */
    public static function md5Pass($password) {
        try {
            return md5($password);
        } catch (\Exception $ex) {
            $_response = array('response' => 'true', 'code' => self::ERRORCODE, 'type' => "CheckRequest", 'errorMessage' => $e->getMessage());
            self::returnJsonResponse($_response, $_type);
            exit;
        }
    }

    /**
     * 
     * @param type $interval
     * @param type $_type
     * @return type
     */
    public static function customDate($interval = 7, $_type="") {
        try {

            return date('Y-m-d', strtotime("+$interval days"));
        } catch (\Exception $ex) {
            $_response = array('response' => 'true', 'code' => self::ERRORCODE, 'type' => "CheckRequest", 'errorMessage' => $e->getMessage());
            self::returnJsonResponse($_response, $_type);
            exit;
        }
    }

    /**
     * 
     * @param type $_epoch
     * @return type
     */
    public static function epochTotime($_epoch) {
        return date('Y-m-d H:i:s', $_epoch);
    }

    /**
     * 
     * @param type $_epoch
     * @return type
     */
    public static function epochToDateOnly($_epoch) {
        return date('Y-m-d', $_epoch);
    }

    /**
     * 
     * @param type $backInterval
     * @param type $_type
     * @return type
     */
    public static function backCustomDate($backInterval = 7, $_type=" ") {
        try {

            return date('Y-m-d H:i:s', strtotime("-$backInterval days"));
        } catch (\Exception $ex) {
            $_response = array('response' => 'true', 'code' => self::ERRORCODE, 'type' => "CheckRequest", 'errorMessage' => $e->getMessage());
            self::returnJsonResponse($_response, $_type);
            exit;
        }
    }

    /**
     * 
     * @param type $string1
     * @param type $string2
     * @return boolean
     */
    public static function equals($string1, $string2) {

        if (strtolower(trim($string1)) == strtolower(trim($string2))) {
            return true;
        }

        return false;
    }

    /**
     * 
     * @throws CHttpException
     */
    public static function siteError() {
        return; //yii 1

        $error = Yii::app()->errorHandler->error;
        if ($error) {
            $_response = array('response' => 'false', 'code' => $error['code'], 'errorMessage' => $error['type']);
            Action::returnJsonResponse($_response);
        } else {
            throw new CHttpException(404, 'Page not found.');
        }
    }

    /**
     * 
     * @param type $msg
     * @throws Exception
     */
    public static function throwException($msg) {
        throw new \Exception($msg);
    }

    /**
     * 
     * @param type $code
     * @return type
     */
    public static function setResponseCode($code) {
        if (empty($code)) {
            return;
        }
        header("HTTP/1.1 $code " . self::$HTTP_CODES[$code] . "");
        http_response_code($code);
    }

    /**
     * 
     * @param type $_type
     * @return type
     */
    public static function getRequest($_type) {

        try {

            return $_SERVER['REQUEST_METHOD'];
        } catch (\Exception $e) {
            $_response = array('response' => 'true', 'code' => self::ERRORCODE, 'type' => "CheckRequest", 'errorMessage' => $e->getMessage());
            self::returnJsonResponse($_response, $_type);
            exit;
        }
    }

    /**
     * 
     * @param type $_type
     */
    public static function checkRequest($_requestType, $_type) {

        try {

            if (!empty($_SERVER['HTTP_ORIGIN'])) {
                
            } else {
                //throw new \Exception('Wrong Integration of API contact Admin');
            }

            if ($_SERVER['REQUEST_METHOD'] !== $_requestType) {
                $_response = array('response' => 'true', 'code' => self::METHODERRORCODE, 'type' => "Method Not Allowed", 'errorMessage' => $_SERVER['REQUEST_METHOD'] . ' is not allowed');
                self::returnJsonResponse($_response);
                exit;
            }
        } catch (\Exception $e) {
            $_response = array('response' => 'true', 'code' => self::ERRORCODE, 'type' => "CheckRequest", 'errorMessage' => $e->getMessage());
            self::returnJsonResponse($_response, $_type);
            exit;
        }
    }

    /**
     * 
     * @param type $val
     * @return string
     */
    public static function generateSession($val = 15) {
        $chars = "abcdef2318ghijklmnopqrstuvwxyz045679ABCDEFGHIJKLMNOPQRSTUVWXYZ,-";
        srand((double) microtime() * 1000000);
        $i = 0;
        $pass = '';
        while ($i <= $val) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $pass = $pass . $tmp;
            $i++;
        }
        return $pass;
    }

    /**
     * 
     * @param type $val
     * @return string
     */
    public static function generateRandomInt($val = 15) {
        $chars = "018001199421964319634199051860472381499734268589205500444127854";
        srand((double) microtime() * 1000000);
        $i = 0;
        $pass = '';
        while ($i <= $val) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $pass = $pass . $tmp;
            $i++;
        }
        return $pass;
    }

    /**
     * (PHP 5 &gt;= 5.3.0, PHP 7)<br/>
     * Check Headers Explicitly
     * @link https://enable-cors.org/server_php.html
     * @param  <p>
     * The header name to be checked.
     * </p>
     * 
     * * @return void No value is returned.
     */
    public static function corsHeader() {

        //header('Access-Control-Allow-Origin: '.$_SERVER['HTTP_ORIGIN']);
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With,Cache-Control,Pragma,Expires,Content-Type, Accept ,sessionToken,userId,refreshToken");
        header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE,OPTIONS,PATCH');


        if (filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_STRING) == 'OPTIONS') {
            exit;
        }
    }

    /**
     * 
     * @param type $name
     * @return boolean
     */
    public static function split_name($name) {
        $parts = array();

        while (strlen(trim($name)) > 0) {
            $name = trim($name);
            $string = preg_replace('#.*\s([\w-]*)$#', '$1', $name);
            $parts[] = $string;
            $name = trim(preg_replace('#' . $string . '#', '', $name));
        }

        if (empty($parts)) {
            return false;
        }

        $parts = array_reverse($parts);
        $name = array();
        $name['first_name'] = $parts[0];
        $name['middle_name'] = (isset($parts[2])) ? $parts[1] : NULL;
        $name['last_name'] = (isset($parts[2])) ? $parts[2] : ( isset($parts[1]) ? $parts[1] : NULL);

        return $name;
    }

    /**
     * 
     * @param type $contentType
     * @return type
     * @throws Exception
     */
    public static function checkHeader($contentType, $type = 'Content Type') {

        try {
            if($_SERVER['REQUEST_METHOD']=='GET'){
                return;
            }
            if (isset($_SERVER['CONTENT_TYPE']) == $contentType) {
                return;
            }
            if (isset($_SERVER['HTTP_CONTENT_TYPE']) == $contentType) {
                return;
            }
            throw new \Exception('Content Type is Not Valid');
        } catch (\Exception $ex) {
            $_response = array('response' => false, 'code' => self::ERRORCODE, 'type' => $type, 'message' => null, 'errorMessage' => $ex->getMessage());
            self::returnJsonResponse($_response);
            exit;
        }
    }

    /**
     * 
     * @param type $response
     * @param type $type
     * @param type $dataCount
     * @param type $status
     * @param type $message
     * @param type $errorMessage
     * @param type $data
     */
    protected function _send_rest_json_response($response, $type, $dataCount = 0, $status = NULL, $message = NULL, $errorMessage = NULL, $data = array()) {
        header("Content-type: application/json");
        $response_data = array(
            'status' => $status,
            'response' => $response,
            'message' => $message,
            'errorMessage' => $errorMessage,
            'type' => $type,
            'dataCount' => $dataCount,
            'data' => $data
        );
        header_remove("Set-Cookie");
        $jsonEncoe = json_encode($response_data);
        print_r($jsonEncoe);
        exit;
    }

    /**
     * 
     * @param type $response
     * @param type $_type
     */
    public static function returnJsonResponse($response, $_type = 'JsonResponse') {
        header("Content-type: application/json");
        //header("Content-type: json");
        header_remove("Set-Cookie");
        $response['system']['logId'] = self::$LOG_ID;
        $response['system']['currentTime'] = self::backCustomDate(0);
        $jsonEncoe = json_encode($response);
        // self::setResponseCode($response['code']);

        @self::afterResponse($jsonEncoe);
        print_r($jsonEncoe);
        exit;
    }

    /**
     * 
     * @param type $response
     * @param type $_type
     */
    public static function setImageContentType($type = 'image/png') {
        header("Content-type: $type");
    }

    /**
     * 
     * @param type $response
     * @param type $_type
     */
    public static function afterResponse($response, $_type = 'JsonResponse') {

        $logs = Logs::findOne(['logRequestId' => self::$LOG_ID]);
        if (empty($logs)) {
            return;
        }

        $logs->response = $response;
        $logs->updatedDate = self::$DATE_TIME;
        $logs->save();
    }

    /**
     * 
     * @param type $url
     * @param type $data
     * @return int
     */
    public static function postData($url, $data, $headers = array('Content-Type:application/json'),$isRet=false) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        
        curl_setopt($ch, CURLOPT_NOBODY,0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $output = curl_exec($ch);
        //$returnStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return $output;
        
        //print_r($output);die;
        
    }
    
    /**
     * 
     * @param type $url
     * @param type $data
     * @param type $headers
     * @return type 
     */
    public static function postDataAuth($url, $data, $username,$password,$headers = array('Content-Type:application/json')) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);  
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $output = curl_exec($ch);
        $returnStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        print_r($output);
        die;

        return $output;
    }

    /**
     * 
     * @param type $url
     * @return boolean
     */
    public static function getHeaderData($url, $headers = array(), $returnCode = 0) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);
        $returnStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($returnCode == 1) {
            return $returnStatus;
        }

        if ($returnStatus == 200) {
            return $output;
        } else {
            return false;
        }
    }

    /**
     * 
     * @return type
     */
    public static function getClientIPaddress() {
        foreach (array('HTTP_CLIENT_IP',
    'HTTP_X_FORWARDED_FOR',
    'HTTP_X_FORWARDED',
    'HTTP_X_CLUSTER_CLIENT_IP',
    'HTTP_FORWARDED_FOR',
    'HTTP_FORWARDED',
    'REMOTE_ADDR') as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $IPaddress) {
                    $IPaddress = trim($IPaddress); // Just to be safe

                    if (filter_var($IPaddress, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false) {

                        return $IPaddress;
                    }
                }
            }
        }

        // For local IP Address
        return $_SERVER['REMOTE_ADDR'];
    }

    /**
     * 
     * @return type
     */
    public static function getClientBrowserDetail() {
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version = "";
        $ub = 'ios';
        //First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        } elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }

        // Next get the name of the useragent yes seperately and for good reason
        if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        } elseif (preg_match('/Firefox/i', $u_agent)) {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        } elseif (preg_match('/Chrome/i', $u_agent)) {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        } elseif (preg_match('/Safari/i', $u_agent)) {
            $bname = 'Apple Safari';
            $ub = "Safari";
        } elseif (preg_match('/Opera/i', $u_agent)) {
            $bname = 'Opera';
            $ub = "Opera";
        } elseif (preg_match('/Netscape/i', $u_agent)) {
            $bname = 'Netscape';
            $ub = "Netscape";
        }

        // finally get the correct version number
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) .
                ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
            // we have no matching number just continue
        }

        // see how many we have
        $i = count($matches['browser']);
        if ($i != 1) {
            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
                $version = isset($matches['version'][0]) ? isset($matches['version'][0]) : '';
            } else {
                $version = isset($matches['version'][1]) ? isset($matches['version'][1]) : '';
            }
        } else {
            $version = $matches['version'][0];
        }

        // check if we have a number
        if ($version == null || $version == "") {
            $version = "?";
        }

        return array(
            'userAgent' => $u_agent,
            'name' => $bname,
            'version' => $version,
            'platform' => $platform,
            'pattern' => $pattern
        );
    }

    /**
     * 
     * @param type $base64_string
     * @return type
     * @throws Exception
     */
    public static function base64($base64_string, $dir = "") {
        // open the output file for writing

        $length = 40;

        try {
            // split the string on commas
            // $data[ 0 ] == "data:image/png;base64"
            // $data[ 1 ] == <actual base64 string>
            $data = explode(',', $base64_string);
            if (empty($data[1])) {
                throw new \Exception("Invalid Image");
            }

            $imageext = explode('/', $data[0]);

            $imageext = explode(';', $imageext[1]);

            $imageext = $imageext[0];

            if ($imageext == "png" || $imageext == "jpg" || $imageext == "jpeg"|| $imageext == "gif") {
                
            } else {
                throw new \Exception('Invalid Image Type');
            }

            $str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $random_front = substr(str_shuffle(str_repeat($x = $str, ceil($length / strlen($x)))), 1, $length);
            $random_back = substr(str_shuffle(str_repeat($x = $str, ceil($length / strlen($x)))), 1, $length);

            $currentDate = date("YmdHis");

            $imageDIR = __DIR__.'/../../media.anothersecondchance.com';
            $_imageName = '/images/' . $dir . $random_front . $currentDate . $random_back . '.' . $imageext;

            $imageName = $imageDIR . $_imageName;
            $ifp = fopen($imageName, 'wb');
            // we could add validation here with ensuring count( $data ) > 1
            fwrite($ifp, base64_decode($data[1]));
            // clean up the file resource
            fclose($ifp);


            return (array('response' => true, 'code' => self::SUCCESSCODE, 'type' => "Image Conversion", "url" => $_imageName, 'errorMessage' => null));
        } catch (\Exception $ex) {

            $_response = array('response' => false, 'code' => self::ERRORCODE, 'type' => 'Image Conversion', 'errorMessage' => $ex->getMessage());
            self::returnJsonResponse($_response);
            exit;
        }
    }

    /**
     * 
     * @param type $page
     * @param type $objectFormation
     * @return type
     * @throws Exception
     */
    public static function data_limit($page, $objectFormation = 0) {

        $page = $page - 1;
        $limit = self::sqlLIMIT;

        if ($page < 1) {
            $page = 1;
            $offset = 0;
        } else {
            $offset = $page * $limit;
        }

        try {

            if ($objectFormation == 1) {
                return ['limit' => $limit, 'offset' => $offset];
            }

            return "LIMIT $limit OFFSET  $offset";
        } catch (\Exception $ex) {
            throw new Exception('Something Went Wrong in Offset');
        }
    }

    /**
     * 
     * @param type $text
     * @param type $encoding
     * @throws Exception
     */
    public static function checkEncoding($text, $encoding) {
        try {
            $val = mb_detect_encoding($text);
            if ($val != $encoding) {
                return FALSE;
            }

            return TRUE;
        } catch (\Exception $ex) {
            throw new Exception('Something Went Wrong while checking String');
        }
    }

    /**
     * 
     * @param type $string
     * @return type
     * @throws Exception
     */
    public static function StringToArray($string) {
        try {

            if (empty($string)) {
                return [];
            }

            return explode(',', $string);
        } catch (\Exception $ex) {
            throw new Exception('Something Went Wrong while checking String');
        }
    }

    /**
     * 
     * @param type $val
     * @return type
     */
    public static function roundUptoTwoDec($val) {
        return round((double) $val, 2);
    }

    /**
     * 
     * @param type $val
     * @return type
     */
    public static function floorUptoTwoDec($val) {
        return floor(self::roundUptoTwoDec($val));
    }

    public static function currentDateOnly() {
        return date('Y-m-d');
    }

    /**
     * 
     * @param type $d1
     * @param type $d2
     * @return type
     */
    public static function dateDiff($d1, $d2) {
        $datetime1 = new \DateTime($d1);
        $datetime2 = new \DateTime($d2);

        $difference = $datetime1->diff($datetime2);
        return $difference->d;
    }

    /**
     * 
     * @param type $a
     * @return type
     */
    public static function convert_array_to_obj_recursive($a) {
        if (is_array($a)) {
            foreach ($a as $k => $v) {
                if (is_integer($k)) {
                    // only need this if you want to keep the array indexes separate
                    // from the object notation: eg. $o->{1}
                    $a[$k] = self::convert_array_to_obj_recursive($v);
                } else {
                    $a[$k] = self::convert_array_to_obj_recursive($v);
                }
            }

            return (array) $a;
        }

        // else maintain the type of $a
        return $a;
    }

    public static function divideBy100($value) {
        return (float) $value / 100;
    }

    /**
     * 
     * @param type $str
     * @return type
     */
    public static function isJson($str) {
        $json = json_decode($str);
        return $json && $str != $json;
    }
    
    public static function notification(NotificationsDAO $obj){
        try{
            
            $notification   = new Notifications();
            
            
            $notification->userMasterId = $obj->getUserMasterId();
            $notification->notificationType = $obj->getNotificationType();
            $notification->url = $obj->getUrl();
            $notification->description = $obj->getDescription();

            $notification->createdBy = $obj->getCreatedBy();
            $notification->updatedBy = $obj->getUpdatedBy();
            $notification->createdDate = $obj->getCreatedDate();
            $notification->updatedDate = $obj->getUpdatedDate();
            $notification->tblId = $obj->getTblId();
            
            $notification->save();
            
        } catch (\Exception $ex) {

        }
    }

}
