<?php

/**
 * 
 * @Author : Saurabh Chhabra <saurabhchhabra018@gmail.com>
 * @Description : ValidatorsController for Input Validates & check user Authenticity
 * 
 * 
 */

namespace app\components;

use Yii;
use yii\web\Controller;
use app\components\AppException as AppException;
use app\models\UserMaster as UserMaster;
use app\models\UserSession as UserSession;
use app\models\PushMaster as PushMaster;

use app\components\GlobalController as Globals;

use app\models\InviteCode as InviteCode;
use app\models\Budget as Budget;
use app\models\ExpenseCategory as ExpenseCategory;

use app\components\v1\messages\Messages as Messages;
use app\models\PetDetailView as PetDetailView;

Class ValidatorsController extends AppException {
    
    
    
    public $maxSize=255;
    
    
    private static $_type = "API_ERR";
    
    public $userStatus = 0;
    
    private static $userObject ;
    private static $petObject;
    
    /**
     * 
     * @return type
     */
    public static function get_type() {
        return self::$_type;
    }

    /**
     * 
     * @param type $_type
     * @return type
     */
    public static function set_type($_type) {
        self::$_type = $_type;
        return self::$_type;
    }
    
    /**
     * 
     * @return type
     */
    public function getUserStatus() {
        return $this->userStatus;
    }

    /**
     * 
     * @param type $userStatus
     * @return $this
     */
    public function setUserStatus($userStatus) {
        $this->userStatus = $userStatus;
        return $this;
    }

    /*
     * 
     */
    public static function getUserObject() {
        return self::$userObject;
    }

    /**
     * 
     * @param type $userObject
     * @return type
     */
    public static function setUserObject($userObject) {
        self::$userObject = $userObject;
        return self::$userObject;
    }
    
    
    public function getMaxSize() {
        return $this->maxSize;
    }

    public static function getPetObject() {
        return self::$petObject;
    }

    public function setMaxSize($maxSize) {
        $this->maxSize = $maxSize;
        return $this;
    }

    public static function setPetObject($petObject) {
        self::$petObject = $petObject;
        return self::$petObject;
    }

    
        
    /**
     * 
     * @param type $intVal
     * @param type $maxSize
     * @param type $type
     * @throws \Exception
     */
    public static function integerType($intVal,$maxSize=20,$type='Validation Failed'){
        try{
            
            $strLen = strlen((string)$intVal);
            
            if($strLen>$maxSize){
                throw new \Exception('Int Too Long');
            }
            
        } catch (\Exception $ex) {
           $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' => $type, 'message'=>null,'errorMessage'=>$ex->getMessage());
           Globals::returnJsonResponse($_response);
           exit;
        }
    }
    
    /**
     * 
     * @param type $intVal
     * @param type $maxSize
     * @param type $type
     * @throws \Exception
     */
    public static function floatType($intVal,$maxSize=20,$type='Validation Failed'){
        try{
            
            $strLen = strlen((string)$intVal);
            
            if($strLen>$maxSize){
                throw new \Exception('Floating Point Accept upto 2 Decimal Places');
            }
            
        } catch (\Exception $ex) {
           $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' => $type, 'message'=>null,'errorMessage'=>$ex->getMessage());
           Globals::returnJsonResponse($_response);
           exit;
        }
    }
    /**
     * 
     * @param type $stringVal
     * @param type $maxSize
     * @param type $type
     */
    public static function stringType($stringVal,$maxSize=255,$type='Validation Failed'){
        try{
            
            $strLen = strlen($stringVal);
            
            if($strLen>$maxSize){
                throw new \Exception('String Too Long');
            }
            
        } catch (\Exception $ex) {
           $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' =>$type , 'message'=>null,'errorMessage'=>$ex->getMessage());
           Globals::returnJsonResponse($_response);
           exit;
        }
    }
    
    
    /**
     * 
     * @param type $stringVal
     * @param type $minSize
     * @param type $type
     * @throws \Exception
     */
    public static function stringTypeMinVal($stringVal,$minSize=255,$type='Validation Failed'){
        try{
            
            $strLen = strlen($stringVal);
            
            if($strLen<$minSize){
                throw new \Exception('String Too Short');
            }
            
        } catch (\Exception $ex) {
           $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' =>$type , 'message'=>null,'errorMessage'=>$ex->getMessage());
           Globals::returnJsonResponse($_response);
           exit;
        }
    }
    
    /**
     * 
     * @param type $val
     * @param type $type
     * @throws \Exception
     */
    public static function booleanType($val,$type='Validation Failed'){
        try{
            $val = (bool)$val;
            if(is_bool($val)==FALSE){
                throw new \Exception('Not a Boolean type');
            }
            
        } catch (\Exception $ex) {
           $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' =>$type , 'message'=>null,'errorMessage'=>$ex->getMessage());
           Globals::returnJsonResponse($_response);
           exit;
        }
    }
    
    /**
     * 
     * @param type $val
     * @param type $type
     * @throws \Exception
     */
    public static function datetimeType($date,$type='Validation Failed',$format = 'Y-m-d H:i:s'){
        try{
            
            $d = \DateTime::createFromFormat($format, $date);
            if($d && $d->format($format) == $date){
                return;
            }else{
                 throw new \Exception('Not a valid Date Time format (Y-m-d H:i:s)');
            }
            
        } catch (\Exception $ex) {
           $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' =>$type , 'message'=>null,'errorMessage'=>$ex->getMessage());
           Globals::returnJsonResponse($_response);
           exit;
        }
    }
    
   /**
    * 
    * @param type $stringVal
    * @param type $minSize
    * @param type $type
    * @throws \Exception
    */
    public static function arrayMaxCount($stringVal,$minSize=255,$type='Validation Failed'){
        try{
            
            $strLen = count($stringVal);
            
            if($strLen<$minSize){
                throw new \Exception("Should be min of $minSize selection");
            }
            
        } catch (\Exception $ex) {
           $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' =>$type , 'message'=>null,'errorMessage'=>$ex->getMessage());
           Globals::returnJsonResponse($_response);
           exit;
        }
    }
    
    /**
     * 
     * @param type $array
     * @param type $type
     * @throws \Exception
     */
    public static function checkIfArray($array,$type='Validation Failed'){
        try{
            
            if(!is_array($array)){
                throw new \Exception("Should be array Type");
            }
            
        } catch (\Exception $ex) {
           $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' =>$type , 'message'=>null,'errorMessage'=>$ex->getMessage());
           Globals::returnJsonResponse($_response);
           exit;
        }
    }
    
    /**
     * 
     * @param type $fbId
     * @param type $type
     */
    public static function validateFBId($fbId,$type='Validation Failed'){
        try{
            if(empty($fbId)){
                throw new \Exception(APIValidations::$statusVar['fbId'].' can not be blank');
            }
            $url = "https://graph.facebook.com/$fbId/picture";
            $code = Globals::getHeaderData($url, $headers=[],1);
            if($code==404){
                throw new \Exception('Invalid FaceBook User');
            }
        } catch (\Exception $ex) {
           $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' =>$type , 'message'=>null,'errorMessage'=>$ex->getMessage());
           Globals::returnJsonResponse($_response);
           exit;
        }
    }
    
    /**
     * 
     * @param type $stringVal
     * @param type $minSize
     * @param type $type
     * @throws \Exception
     */
    public static function checkImei($stringVal,$minSize=255,$type='Validation Failed'){
        try{
            
            $strLen = strlen($stringVal);
          
            if($strLen!=$minSize){
                throw new \Exception("String should be exact $minSize characters");
            }
            
            self::imeiLuhAlgo($stringVal, $type);
            
        } catch (\Exception $ex) {
           $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' =>$type , 'message'=>null,'errorMessage'=>$ex->getMessage());
           Globals::returnJsonResponse($_response);
           exit;
        }
    }
    
    
    protected static function imeiLuhAlgo($stringVal,$type='Validation Failed') {

        try {


            $strSplit = str_split($stringVal);
            $result_array = array();
            foreach ($strSplit as $each_number) {
                $result_array[] = (int) $each_number;
            }


            //print_r($result_array);
            for ($i = 1; $i < count($result_array); $i += 2) {
                $result_array[$i] = $result_array[$i] * 2;
                $strCheck = strlen((string) $result_array[$i]);
                if ($strCheck > 1) {
                    $strSpliter = str_split($result_array[$i]);
                    $sumStr = 0;
                    foreach ($strSpliter as $strChecker) {
                        $sumStr = (int) $strChecker + $sumStr;
                    }

                    $result_array[$i] = $sumStr;
                }
            }

            $sumImei = 0;
            foreach ($result_array as $imeiAlgo) {
                $sumImei = $sumImei + $imeiAlgo;
            }


            if ($sumImei % 10 != 0) {
                throw new \Exception('Incorrect IMEI');
            }
            
            return;
            
            
        } catch (\Exception $ex) {
           $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' =>$type , 'message'=>null,'errorMessage'=>$ex->getMessage());
           Globals::returnJsonResponse($_response);
           exit;
        }
    }

    /**
    * <p style='color:green;'>Non Mandatory Fields to null values</p>
    * <p style='color:red;'>and other to trim functions so that every values will be trimmed</p>
    * @param type $data
    * @param type $arrayNULLVal 
    * @param type $_type
    * 
    */
    public static function checkIsNullExp($data, $arrayNULLVal = array(),$_type) {

        try {
            if (is_array($arrayNULLVal)) {
                foreach ($arrayNULLVal as $key) {
                    if (!array_key_exists($key, $data)) {
                        $data[$key] = null;
                    }else{
                        if(!is_array($data[$key]))
                            $data[$key] = trim($data[$key]);
                    }
                }
            }
            return $data;
        } catch (\Exception $ex) {
            $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' => $_type, 'message' => null, 'errorMessage' => $ex->getMessage());
            Globals::returnJsonResponse($_response);
            exit;
        }
    }
    
    /**
     * 
     * @param type $partnerUserMasterId
     * @param type $_type
     */
    public static function checkPartner($partnerUserMasterId,$_type){
        try {
           
            $partner = InviteCode::findOne(['firstSpouse' => self::$userObject->userId,'secondSpouse'=>$partnerUserMasterId, 'status' => '1']);
            if(!empty($partner)){
                return;
            }
            
            $partner = InviteCode::findOne(['secondSpouse' => self::$userObject->userId,'firstSpouse'=>$partnerUserMasterId, 'status' => '1']);
            if(!empty($partner)){
                return;
            }
            
            throw new \Exception("Not a valid Partner");
            
        } catch (\Exception $ex) {
            $_response = array('response' => false, 'code' => Globals::ERRORCODE, 'type' => $_type, 'message' => null, 'errorMessage' => $ex->getMessage());
            Globals::returnJsonResponse($_response);
            exit;
        }
    }
    
    
    /**
     * 
     */
    public static function checkInActiveUserSession(){
        try{
            
           return self::checkUserSession($_type='AuthLayer', $sessionToken = NULL, $refreshToken = NULL,$roleName=Globals::USER,$isActive=0);
            
        } catch (\Exception $ex) {
            $_response = array('response' => false, 'code' => Globals::USERSESSIONEXPIRED, 'type' => $_type, 'message' => null, 'errorMessage' => $ex->getMessage(),'_internalMessage'=>"The client's session has expired and must log in again");
            Globals::returnJsonResponse($_response);
            exit;
        }
    }
    
    public static function checkUserPet($petId = 0){
        try {
            
             
            if($petId==0 || empty($petId)){
                throw new \Exception(Messages::noPetProfile);
            }
            
            $petObj = PetDetailView::findOne(['petId'=>$petId]);
            if(empty($petObj)){
                throw new \Exception(Messages::noPetProfile);
            }
            
            self::setPetObject($petObj);
            return $petObj->petId;
   
        } catch (Exception $ex) {
             $_response = array('response' => false, 'code' => Globals::USERSESSIONEXPIRED, 'type' => self::get_type() , 'message' => null, 'errorMessage' => $ex->getMessage()
                    ,'_internalMessage'=>Messages::noPetProfile);
            Globals::returnJsonResponse($_response);
            exit;
        }
    }
    
    /**
     * 
     * @param type $um
     * @param type $roleName
     * @return type
     * @throws \Exception
     */
    public static function roleMatrix($um, $roleName) {        
        
        if($um->roleName == $roleName){
            return;
        }
        switch($roleName){
        case Globals::USER:
            if($um->roleName == $roleName){
                return;
            }else if($um->roleName == Globals::ADMIN){
                return;
            }
            break;
        case Globals::ADMIN:
            if($um->roleName == $roleName){
                return;
            }
            break;
        }
        
        throw new \Exception('Not a Valid User');

    }

    /**
     * 
     * @param type $_type
     * @return type
     * @throws \Exception
     */
    public static function checkUserSession($_type='AuthLayer',$roleName=null,$post=0,$isActive=1){
        try{
            
           $_deviceToken  = isset($_SERVER["HTTP_DEVICETOKEN"]) ? $_SERVER["HTTP_DEVICETOKEN"] : NULL;
                $_deviceUUID  = isset($_SERVER["HTTP_DEVICEUUID"]) ? $_SERVER["HTTP_DEVICEUUID"] : -1;
                $_deviceName  = isset($_SERVER["HTTP_DEVICENAME"]) ? $_SERVER["HTTP_DEVICENAME"] : NULL;
            if($post==1){
                $userId         = isset($_POST["userid"]) ? $_POST["userid"] : Globals::throwException('Invalid User');
                $_sessionToken  = isset($_POST["sessionToken"]) ? $_POST["sessionToken"] : Globals::throwException('Invalid Token');
                $_refreshToken  = isset($_POST["refreshToken"]) ? $_POST["refreshToken"] : NULL;  
            }else{
                 $userId         = isset($_SERVER["HTTP_USERID"]) ? $_SERVER["HTTP_USERID"] : Globals::throwException('Invalid User');
                $_sessionToken  = isset($_SERVER["HTTP_SESSIONTOKEN"]) ? $_SERVER["HTTP_SESSIONTOKEN"] : Globals::throwException('Invalid Token');
                $_refreshToken  = isset($_SERVER["HTTP_REFRESHTOKEN"]) ? $_SERVER["HTTP_REFRESHTOKEN"] : NULL;

                
            
            }
            
            $sessionToken   =  $_sessionToken;
            $refreshToken   =  $_refreshToken;
            $currentDate = date("Y-m-d H:i:s");
            
            if(empty($sessionToken)){
                throw new \Exception('Session Token Expired . Kindly Login Again');
            }
            
            $um = UserMaster::findOne(['userId'=>$userId,'status'=>'1']);            
            if(empty($um)){
                 throw new \Exception('Not a Valid User');
            }
            
            if(!empty($roleName)){
                self::roleMatrix($um,$roleName);
            }
            
            
            self::setUserObject($um);
            
            $sess = UserSession::findOne(['userMasterId'=>$userId,'sessionToken'=>$sessionToken,'status'=>$isActive]);
            
            if($isActive==0){
                self::regainLostSession($sess);
            }
            
            if(empty($sess)){
                throw new \Exception("The client's session has expired and must log in again");
            }

            if($sess->expiryDate < Globals::backCustomDate(30,$_type)){
                $extendSession              = UserSession::findOne(['userMasterId'=>$userId,'sessionToken'=>$sessionToken,'refreshToken'=>$refreshToken,'status'=>'1']);
                if(empty($extendSession)){
                    throw new \Exception("The client's session has expired and must log in again");
                }
                $extendSession->expiryDate  = Globals::customDate(30,$_type);
                $extendSession->updatedDate = $currentDate;
                $extendSession->save();
            }
            
            if(!empty($_deviceName) && !empty($_deviceToken)){
                self::PushToken($userId, $_deviceToken, $_deviceUUID, $_deviceName,$currentDate, $_type);
            }
            
            
            if($post==1){
                 return ['status'=>true,'message'=>''];
            }
            return $userId;
            
        } catch (\Exception $ex) {
            if($post==1){
                 return ['status'=>false,'message'=>$ex->getMessage()];
            }
            $_response = array('response' => false, 'code' => Globals::USERSESSIONEXPIRED, 'type' => $_type, 'message' => null, 'errorMessage' => $ex->getMessage()
                    ,'_internalMessage'=>"The client's session has expired and must log in again");
            Globals::returnJsonResponse($_response);
            exit;
        }
    }
    
    /**
     * 
     * @param type $_type
     * @param type $roleName
     * @throws \Exception
     */
    public static function checkAdminSession($_type='AdminAuthLayer',$roleName=Globals::ADMIN){
        try{
            
            $userId = self::checkUserSession($_type,$roleName);
            
            if(empty($userId)){
                 throw new \Exception('Not a Valid User');
            }
            
            return $userId;
            
        } catch (\Exception $ex) {
            $_response = array('response' => false, 'code' => Globals::USERSESSIONEXPIRED, 'type' => $_type, 'message' => null, 'errorMessage' => $ex->getMessage(),'_internalMessage'=>"The client's session has expired and must log in again");
            Globals::returnJsonResponse($_response);
            exit;
        }
    }
    
    /**
     * 
     * @param type $sessionObj
     * @return type
     */
    public static function regainLostSession($sessionObj){
        try{
            
            $sessionObj->status = 1;
            $sessionObj->save();
            
            return;
            
        } catch (\Exception $ex) {
            $_response = array('response' => false, 'code' => Globals::USERSESSIONEXPIRED, 'type' => self::get_type(), 'message' => null, 'errorMessage' => $ex->getMessage(),'_internalMessage'=>"The client's session has expired and must log in again");
            Globals::returnJsonResponse($_response);
            exit;
        }
    }
    
    /**
     * 
     * @param type $userId
     * @return type
     */
    public static function endSession($userId){
        try{
            
            $_sessionToken  = isset($_SERVER["HTTP_SESSIONTOKEN"]) ? $_SERVER["HTTP_SESSIONTOKEN"] : Globals::throwException('Invalid Token');
            $sessionObj = UserSession::findOne(['userMasterId'=>$userId,'sessionToken'=>$_sessionToken,'status'=>'1']);
            
            $sessionObj->status = 0;
            $sessionObj->save();
            
            return;
            
        } catch (\Exception $ex) {
            $_response = array('response' => false, 'code' => Globals::USERSESSIONEXPIRED, 'type' => self::get_type(), 'message' => null, 'errorMessage' => $ex->getMessage(),'_internalMessage'=>"The client's session has expired and must log in again");
            Globals::returnJsonResponse($_response);
            exit;
        }
    }

        
    /**
     * 
     * @param type $_type
     * @param type $sessionToken
     * @param type $refreshToken
     * @param type $roleName
     */
    public static function checkUserSessionOnExist($_type='AuthLayer', $sessionToken = NULL, $refreshToken = NULL,$roleName=Globals::USER){
        try{
            
            $userId         = isset($_SERVER["HTTP_USERID"])?$_SERVER["HTTP_USERID"]:NULL;
            
            if($userId==null){
                return -1;
            }
            
            $userId=self::checkUserSession($_type, $sessionToken, $refreshToken, $roleName);
            return $userId;
            
        } catch (\Exception $ex) {
            $_response = array('response' => false, 'code' => Globals::USERSESSIONEXPIRED, 'type' => $_type, 'message' => null, 'errorMessage' => $ex->getMessage(),'_internalMessage'=>"The client's session has expired and must log in again");
            Globals::returnJsonResponse($_response);
            exit;
        }
    }
    
    /**
     * 
     * @param type $userId
     * @param type $_deviceToken
     * @param type $_deviceUUID
     * @param type $_deviceName
     * @param type $date
     * @param type $_type
     */
    public static function PushToken($userId,$_deviceToken,$_deviceUUID,$_deviceName,$date,$_type){
        try{
            
            $push = PushMaster::findOne(['userMasterId'=>$userId,'status'=>'1']);
            if(empty($push)){
                $push = new PushMaster();
                $push->userMasterId     = $userId;
                $push->createdDate      = $date;
            }
            
            $push->deviceToken          = $_deviceToken;
            $push->deviceUUID           = $_deviceUUID;
            $push->deviceName           = $_deviceName;
            
            $push->updatedDate          = $date;
            
            $push->save();
            
        } catch (Exception $ex) {
             
            $_response = array('response' => false, 'code' => Globals::USERSESSIONEXPIRED, 'type' => $_type, 'message' => null, 'errorMessage' => $ex->getMessage(),'_internalMessage'=>"The client's session has expired and must log in again");
            Globals::returnJsonResponse($_response);
            exit;
        }
    }

}
