<?php

$params = require __DIR__ . '/params.php';
//$db = require __DIR__ . '/db.php';

/**
 *  DB CONFIG
 */
switch ($_SERVER['SERVER_NAME']) {


    case 'localhost':
        Yii::$app->params['p0'] = "http://localhost:8282";
        $db = require __DIR__ . '/db/db.php';
        $debug = TRUE;
        break;
    case '192.168.10.13':
        Yii::$app->params['p0'] = "http://localhost:8282";
        $db = require __DIR__ . '/db/db.php';
        $debug = TRUE;
        break;

    default:
        Yii::$app->params['p0'] = "http://localhost:8282";
        $db = require __DIR__ . '/db/dev.php';
        $debug = TRUE;
        break;
}

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<param1:\w+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<param1:\d+>/<param2:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<param1:\w+>/<param2:\w+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<param1:\d+>/<param2:\d+>/<param3:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<param1:\w+>/<param2:\w+>/<param3:\w+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<param1:\w+>/<param2:\w+>/<param3:\w+>/<param4:\w+>' => '<controller>/<action>',
		'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
        
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'saurabh123',
            'enableCsrfValidation' => false,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            //'useFileTransport' => true,
            'enableSwiftMailerLogging' => true,
            //'emailuser' => 'noreply.redash.saurabh.flip@gmail.com',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host'=>'smtp.gmail.com',
                'username'=>'',
                'password'=>'',
                //'port'=>'25',          
                'port' => '587', // Port 25 is a very common port too
                'encryption' => 'tls', // It is often used, check your provider or mail server specs
             ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
