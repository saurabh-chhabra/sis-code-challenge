
/** DB SQL **/

-- 16-MAR-2018

CREATE TABLE `income_categories` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userMasterId` BIGINT NOT NULL,
  `categoryName` VARCHAR(255) NULL,
  `status` TINYINT(2) NULL DEFAULT '1' COMMENT 'is user can login :: database validation :: soft delete',
  `createdBy` BIGINT(20) NULL DEFAULT NULL,
  `updatedBy` BIGINT(20) NULL DEFAULT '-1' COMMENT 'updated by user_id',
  `createdDate` DATETIME NULL DEFAULT NULL COMMENT 'created_date of insertion',
  `updatedDate` TIMESTAMP NULL DEFAULT NULL COMMENT 'updation date of row',
  PRIMARY KEY (`id`),
  INDEX `idx_user_master_id` (`userMasterId` ASC));
  
  
  ALTER TABLE `expense_db`.`income_categories` 
  ADD CONSTRAINT `fk_user_id_cat_id`
  FOREIGN KEY (`userMasterId`)
  REFERENCES `expense_db`.`user_master` (`userId`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;



/** **/