<p align="center">
    <h1 align="center">SIS Code Readme</h1>
    <br>
</p>


DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources


	  
1. Clone the project
   Import db from db_bk/ folder with 
   database name "framework_test" 
   user : root
   pass : root
   or change config via config/db/int.php
   
2. run via composer "composer install"
3. composer will install all dependencies in vendor/ folder or for quick action vendor.zip contains all dependencies just unzip it
4. run for cmd or shell php local server command for project main structure
   php -S localhost:9191 -t web/
   
5. In any web platform open http://localhost:9191/site , login page will open
Admin Login : admin
Admin Pass : 123456

USER LOGIN : user1@example.com
USER PASS : 123456

REGISTERATION LINK 
http://localhost:9191/site/register

6. After sucessful login admin can see all user dashboard/reporting on http://localhost:9191/site/dashboard link , user(s) can aslo see respected data on this link only (respected login id)
7. Admin can upload expense sheet via http://localhost:9191/site/upload
8. Admin Can see all user list on current system http://localhost:9191/site/user
9. Admin can all users expenses via http://localhost:9191/site/expense
10. User can see his/her expenses that has been uploaded by admin

PSV must be in this format
date|category|employee_email|employee_address|expense_description|pre_tax_amount|tax_amount

I have tweaked one column previosly called employee_name to employee_email as system must understand that this user is unique to user same name can be repeat in big organisation but email-id will be unique
So psv file must have some concrete key in which each row can be distinguished


I proud to implement api level in this will user specific api's no one will forged session can pass into system ,even uploads . I have implented API's with strong checking of REQUEST_METHODS ,SESSION_TOKEN,REFRESH_TOKEN,USERID & api also has user role matrix auth .


Are all required functions enabled/functioning?
This is Yii framework all functions are done in order to do some micro to big task .So all functions are working

Did you include build/deploy instructions and your note of what you did well? 
Yes included

Are any models/entities/components easily identifiable to the reviewer?
Models / components are easily identifiable by reviewer in /model & /components folder respectively

Are the design decisions you made sound/warranted in your models/entities? Why (i.e. were they explained?)
yes they are well expalined as I am using namespaces so every things is upto the mark

Does the solution use appropriate datatypes for the problem as described?
As php is lossely types so in models I have made DAO (data acess object) which will have type conversions in it ,if required

