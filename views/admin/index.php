<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Travel Unc Application';
//echo "<pre>";print_r($model);
?>

<div class="site-index">
	<div class="body-content">
				<div class="row">	
				<div class="col-lg-4">
				<h2>Sign In</h2>
				<?php
				$form = ActiveForm::begin(['id' => 'login-form']);
				echo $form->field($model, 'username');
				echo $form->field($model, 'password')->passwordInput();
				echo $form->field($model, 'rememberMe')->checkbox();
				echo Html::submitButton(
				'Login', 
				['class' => 'btn btn-primary', 'name' => 'login-button']
				);
				ActiveForm::end();

				?>
				</div>
					   
				</div>
	</div>
</div>
