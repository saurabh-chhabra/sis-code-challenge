<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

  <div class="page-wrapper chiller-theme toggled">
    <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
      <i class="fas fa-bars"></i>
    </a>
    <?php require_once(__DIR__.'/../layouts/header.php'); ?>
    <main class="page-content">
      <div class="container-fluid">
        <section id="tabs" class="project-tab">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="project-heading">
                 
                  <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="/site/dashboard">Dashboard</a></li>
                      <li class="breadcrumb-item active" aria-current="page">User Expense Data</li>
                    </ol>
                  </nav>
                </div>
                <nav>
                  <!-- <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab"
                      aria-controls="nav-home" aria-selected="true">All</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab"
                      aria-controls="nav-profile" aria-selected="false">Pending</a>
                  </div> -->
                </nav>
                <div class="tab-content" id="nav-tabContent">
                  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div class="table-section">

                      <!-- <h6>Request Pending for Approval</h6> -->
                      <div id="example_wrapper" class="dataTables_wrapper table-responsive">
                        <table id="datatable" class="display" style="width:100%">
                          <thead>
                            <tr>
                            <tr>
                              <th>

                              </th>
                              <th>
                                Name
                              </th>
                              <th>
                                UUID
                              </th>
                              <th>
                                Date
                              </th>
                              <th>
                                category
                              </th>
                              <th>
                                Employee Address
                              </th>
                             
                              <th>
                                Expense Description
                              </th>
                              <th>
                                Pre Tax Amount
                              </th>
                              <th>
                                Tax Amount
                              </th>
                              <th>
                                Total Amount
                              </th>
                              
                            </tr>
                          </thead>
                          <tbody id="list">
                           
                          </tbody>
                          <tfoot>
                            <tr>
                            <tr>
                             <th>

                              </th>
                              <th>
                                Name
                              </th>
                              <th>
                                UUID
                              </th>
                              <th>
                                Date
                              </th>
                              <th>
                                category
                              </th>
                              <th>
                                Employee Address
                              </th>
                             
                              <th>
                                Expense Description
                              </th>
                              <th>
                                Pre Tax Amount
                              </th>
                              <th>
                                Tax Amount
                              </th>
                              <th>
                                Total Amount
                              </th>
                            </tr>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
    </main>

    <!-- page-content" -->
  </div>

<?php require_once(__DIR__.'/../layouts/alerts/confirm.php'); ?>

<script>

  function getAllExpense() {
    var refreshToken = localStorage.getItem("refreshToken");
    var sessionToken = localStorage.getItem("sessionToken");
    var userId = localStorage.getItem("userMasterId");
    var roleName = localStorage.getItem("roleName");
    
    if(roleName=="ADMIN"){
        $api = "admin";
    }if(roleName=="USER"){
        $api = "v1";
    }

    $.ajax({
      type: "GET",
      beforeSend: function (request) {
        request.setRequestHeader("userId", userId);
        request.setRequestHeader("refreshToken", refreshToken);
        request.setRequestHeader("sessionToken", sessionToken);
      },
      url: API_URL +$api+"/getAllExpenses",
      dataType: "json",
      contentType: "application/json",
      timeout: 8000,
      success: function (data) {
        //console.log(data);
        if (data.code == 200) {
          setTableData(data);
        } else {
          ERRMSG("Get All Expense", data.errorMessage);
          return [];
        }
      },
      error: function (x, t, m) {
        if (t === "timeout") {
          consoleWarn("WARN : ", "Server did not respond. Please try later.");
          return [];
        }
        ERRMSG("Get All Users", x.responseJSON.errorMessage);
      }
    });
  }


  function setTableData(data) {
    var appendHTML = "";
    var index = 0;
    allUserdata = data.data.list;

    allUserdata.forEach(element => {

      var status = 'Active';
      var delTxt = 'De-Active';
      if (!element.status) {
        status = 'In-Active';
        delTxt = 'Re-Activate';
      }

      appendHTML += '<tr>'

        + '<td class="py-1" >'
        + '<img src="' + element.profilePic + '" alt="image" /></td>'
        + '<td>' + element.fullName + '</td>'
        + '<td>' + element.user_id + '</td>'
        + '<td>' + element.date + '</td>'
        + '<td>' + element.category + '</td>'
        + '<td>' + element.employee_address + '</td>'
        +'<td>'+element.expense_description+'</td>'
        +'<td>$'+element.pre_tax_amount+'</td>'
        +'<td>$'+element.tax_amount+'</td>'
        +'<td>$'+(parseFloat(element.pre_tax_amount) + parseFloat(element.tax_amount))+'</td>'
        + '</tr>';

      ++index;
    });
    //consolelog(appendHTML);
    $('#datatable').DataTable().destroy();
   
    
    $("#list").html(appendHTML);
    $('#datatable').DataTable({ 
    });

  }

  window.onload = function(){getAllExpense();}
</script>

