<?php
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

<div class="page-wrapper chiller-theme toggled">
    <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
        <i class="fas fa-bars"></i>
    </a>
    <?php require_once(__DIR__ . '/../layouts/header.php'); ?>
    <main class="page-content">
        <div class="container-fluid">
            <section id="tabs" class="project-tab">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="project-heading">

                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/site/dashboard">Dashboard</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">User Expense Sheet</li>
                                    </ol>
                                </nav>
                            </div>
                            <nav>
                                <!-- <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                  <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab"
                                    aria-controls="nav-home" aria-selected="true">All</a>
                                  <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab"
                                    aria-controls="nav-profile" aria-selected="false">Pending</a>
                                </div> -->
                            </nav>
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                    <div class="table-section">

                                        <!-- <h6>Request Pending for Approval</h6> -->
                                        <div id="example_wrapper" class="dataTables_wrapper table-responsive">
                                            <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" enctype="multipart/form-data" id="uploadform" class="uploadform text-center">
                                               
                                                <label class="expensetitle mb-4"><u>Upload Expense Sheet</u></label><br/>
                                                <div class="mb-3"><input type="file" name="file" id="file" class="file" accept=".psv"/></div>
                                                <progress id="progressBar" class="progressBar" value="0" max="100" style="width:300px;"></progress>
                                                <div class="progress" style="display: none;">
                                                    <div class="bar"></div >
                                                    <div class="percent">0%</div >
                                                </div>
                                                <input type="hidden" name="refreshToken" id="refreshToken" value="" />
                                                 <input type="hidden" name="userid" id="userid"  value='' />
                                                  <input type="hidden" name="sessionToken" id="sessionToken"  value='' />
                                                  <br/><div class="" style="margin-top:20px"><input class="btn btn-primary" type="submit" name="submit" value="Upload" /></div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
    </main>

    <!-- page-content" -->
</div>

<?php require_once(__DIR__ . '/../layouts/alerts/success.php'); ?>

<script>
 
document.getElementById("file").value = "";

<?php if ($message != null || $message != "") { ?>
        window.onload = function () {
           
            loadadmin();
            $('.successTitle').html("Expense Sheet");
            $('.successMessage').html("<?php echo $message;?>");
            $('#success-alert').modal();
            
            if ($(".uploadform").length > 0) {
                $(".uploadform")[0].reset();
                document.getElementById("file").value = "";
            }
        };
<?php }else{?>
    window.onload = function () { loadadmin(); }
<?php } ?>


function loadadmin(){
      $("#userid").val(localStorage.getItem("userMasterId"));
      $("#sessionToken").val(localStorage.getItem("sessionToken"));
      $("#refreshToken").val(localStorage.getItem("refreshToken"));
      
      
    var bar = $('.bar');
    var percent = $('.percent');
    var status = $('#status');
    var progress_bar = $(".progressBar");

    $('form').ajaxForm({
        beforeSend: function() {
            status.empty();
            var percentVal = '0%';
            bar.width(percentVal);
            percent.html(percentVal);
            progress_bar.val(0);
            //progress_bar.css("width", percentVal);
        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal);
            percent.html(percentVal);
            progress_bar.val(percentComplete);
            //progress_bar.css("width", percentVal);
        },
        complete: function(xhr) {
            status.html(xhr.responseText);
            $('.successTitle').html("Expense Sheet");
            $('.successMessage').html("Successfully Uploaded Expense Sheet");
            $('#success-alert').modal();
        }
    });
}

</script>

