<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
   <div class="login-home">
      <div class="container">
        <div class="login-content">
          <div class="location">
            <img src="/images/blank.png" alt="APP LOGO" />
          </div>

          <div class="login-welcome">
            <h1>Registration</h1>
            <h5>Please fill in details to get registered</h5>
          </div>

          <div class="login-form">
            <div class="registration-form">
              <div>
                <div class="form-group">
                  <label>Full Name</label>
                  <input
                    type="text"
                    class="form-control"
                    id="fullname"
                    required=""
                    onkeypress="(event.keyCode === 13)?login():false;"
                  />
                </div>
                <div class="form-group">
                  <label>Email Address</label>
                  <input
                    type="email"
                    class="form-control"
                    id="loginEmail"
                    required=""
                    onkeypress="(event.keyCode === 13)?login():false;"
                  />
                </div>
                <div class="form-group">
                  <label>Password</label>
                  <input
                    type="password"
                    class="form-control"
                    id="loginPassword"
                    required=""
                  />
                </div>
                
                <div class="form-group">
                  <label> Confirm Password</label>
                  <input
                    type="password"
                    class="form-control"
                    id="cloginPassword"
                    required=""
                  />
                </div>

                <button
                  type="submit"
                  id="submitBtn"
                  onclick="login()"
                  class="sign-in"
                >
                  Register
                </button>

                <div class="forgot">
                  <a href="/site/index">Existing User?</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php require_once(__DIR__ . '/../layouts/alerts/error.php'); ?>
<?php require_once(__DIR__ . '/../layouts/alerts/success.php'); ?>
<script>

function login() {
  var jsonData = {
    emailId: document.getElementById("loginEmail").value,
    password: document.getElementById("loginPassword").value,
    cpassword: document.getElementById("cloginPassword").value,
    name: document.getElementById("fullname").value,
  };
  
  if(jsonData.password != jsonData.cpassword){
      ERRMSG("User Register","Password & Confirm Password doesn't match");
      return;
  }

  //header =request.overrideMimeType("application/j-son;charset=UTF-8");

  $.ajax({
    type: "POST",
    beforeSend: function (request) { },
    url: API_URL + "v1/userSignUp",
    dataType: "json",
    data: JSON.stringify(jsonData),
    contentType: "application/json",
    timeout: 8000,
    success: function (data) {
      //console.log(data);
      if (data.code == 200) {
        SUCCESSMSG("User Register",data.message); 
        document.getElementById("forgot-btn").addEventListener("click", redirectToLogin);
      } else {
        ERRMSG("User Register",data.errorMessage);
      }
    },
    error: function (x, t, m) {
      if (t === "timeout") {
        consoleWarn("WARN : ", "Server did not respond. Please try later.");
      }
    }
  });

}

function redirectToLogin(){
    window.location.href = "/site/index";
}
</script>
