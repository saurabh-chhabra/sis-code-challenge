<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$this->title = 'API | Getter';
?>

<!DOCTYPE html>
<html>
    <head>
        <style>
            html,body{
                    margin-left: -300px !important;
            }
            table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
            }

            tr:nth-child(even) {
                background-color: #dddddd;
            }
            
            .wraper {
                word-wrap: break-word;
            }
        </style>
    </head>
    <body>

        <table>
            <tr>
                <th>#</th>
                <th>SampleDomain</th>
                <th>ApiName</th>
                <th>ScriptName</th>
                <th>ApiType</th>
                <th>Header</th>
                <th>Request</th>
                <th>Response</th>

            </tr>
            <?php 
            
            header('Content-Type: application/json');
            
            foreach ($model as $_m) : ?>
                <tr>
                    <td><?php echo $_m['id']; ?></td>
                    <td><?php echo $_m['sample_domain']; ?></td>
                    <td><?php echo $_m['api_name']; ?></td>
                    <td><?php echo $_m['script_name']; ?></td>
                    <td><?php echo $_m['api_type']; ?></td>
                    <td><pre><?php echo $_m['header']; ?></pre></td>
                    <td><pre><?php print_r($_m['request']);?></pre></td>
                    <td ><pre><?php print_r($_m['response']);?></pre></td>
                </tr>
            <?php endforeach; ?>
        </table>

    </body>
</html>

