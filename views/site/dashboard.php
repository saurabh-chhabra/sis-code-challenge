<?php
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

<div class="page-wrapper chiller-theme toggled">
    <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
        <i class="fas fa-bars"></i>
    </a>
    <?php require_once(__DIR__ . '/../layouts/header.php'); ?>
    <main class="page-content">
        <div class="container-fluid">
            <div style="margin:30px;">

                <!--   -->
                <div class="row" style="margin: 70px 0;">
                        <div class="col-md-4 border-box">
                            <p class="heading">Current Month Expenses </p>
                            <p style="color:#53a7a5; font-size:20px; font-weight: bold;"><span>USD </span><span id="m"></span></p>
                        </div>
                        <div class="col-md-4 border-box">
                            <p class="heading">Current Yearly Expenses </p>
                            <p style="color:#53a7a5; font-size:20px; font-weight: bold;"><span>USD </span><span id="y"></span></p>
                        </div>
                        <div class="col-md-4 border-box">
                            <p class="heading">Total Expenses </p>
                            <p style="color:#53a7a5; font-size:20px; font-weight: bold;"><span>USD </span><span id="all"></span></p>
                        </div>
                </div>
                <div class="row" style="margin-bottom: 20px;margin-left: 200px;font-size: 22px;"><b>Expense Graph</b></div>
                <div class="row" style="margin-bottom: 50px;margin-left: 150px;"><div id="year-month"></div></div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="report-chart" style="text-align: center;">
                            <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                        </div>
                    </div>


                </div>
                 <div class="row">
                    <div class="col-md-12">
                        <div class="report-chart">
                            <div id="chartContainer1" style="height: 300px; width: 100%;"></div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!-- table section -->




    </main>

    <!-- page-content" -->

    <!-- page-wrapper -->

    <script>
        // #DEEBAC
        var _analytics = {};
        var headText = "";

        var temp = 0;
        var finance = [];
        var cat = [];

        function explodePie(e) {
            for (var i = 0; i < e.dataSeries.dataPoints.length; i++) {
                if (i !== e.dataPointIndex)
                    e.dataSeries.dataPoints[i].exploded = false;
            }
        }

        function analytics() {
            var refreshToken = localStorage.getItem("refreshToken");
            var sessionToken = localStorage.getItem("sessionToken");
            var userId = localStorage.getItem("userMasterId");
            var roleName = localStorage.getItem("roleName");
            finance = [];
            cat = [];
            
            $api = "admin";
            if (roleName == "ADMIN") {
                $api = "admin";
            }
            if (roleName == "USER") {
                $api = "v1";
                headText = "";
            }

            $.ajax({
                type: "GET",
                beforeSend: function (request) {
                    request.setRequestHeader("userId", userId);
                    request.setRequestHeader("refreshToken", refreshToken);
                    request.setRequestHeader("sessionToken", sessionToken);
                },
                url: API_URL + $api + "/analytics?" + "year=" + $('#multiDrop').val() + "&month=" + $('#multiDrop2').val(),
                dataType: "json",
                contentType: "application/json",
                timeout: 8000,
                success: function (data) {
                    //console.log(data);
                    if (data.code == 200) {
                        _analytics = data.data.list;

                        for (var k = 0; k < _analytics.allexpense.length; k++) {
                            var tmpobj = [];
                            tmpobj['x'] = new Date(_analytics.allexpense[k].date);
                            //tmpobj['x']=(_analytics.allexpense[k].category);
                            tmpobj['y'] = parseFloat(_analytics.allexpense[k].total);
                            tmpobj['color'] = "#CB4845";
                            //tmpobj['label']= _analytics.allexpense[k].category;

                            finance.push(tmpobj);
                        }

                        salesPerMonth(finance);
                        
                        
                        for (var k = 0; k < data.data.expense.cat.length; k++) {
                            var tmpobj = [];
                            tmpobj['label'] = data.data.expense.cat[k].x;
                            //tmpobj['x']=(_analytics.allexpense[k].category);
                            tmpobj['y'] = round(data.data.expense.cat[k].y);
                            
                            //tmpobj['label']= _analytics.allexpense[k].category;

                            cat.push(tmpobj);
                        }
                        loadCategory(cat);

                        var expenses = data.data.expense;
                        if (expenses.all == null) {
                            $("#all").html("0");
                        } else {
                            $("#all").html(round(expenses.all.totalexpense));
                        }

                        if (expenses.m == null) {
                            $("#m").html("0");
                        } else {
                            $("#m").html(round(expenses.m.totalexpense));
                        }

                        if (expenses.y == null) {
                            $("#y").html("0");
                        } else {
                            $("#y").html(round(expenses.y.totalexpense));
                        }
                        console.log(finance);

                    } else {
                        ERRMSG("Get Contact", data.errorMessage);
                        return [];
                    }
                },
                error: function (x, t, m) {
                    if (t === "timeout") {
                        consoleWarn("WARN : ", "Server did not respond. Please try later.");
                        return [];
                    }
                }
            });
        }

        function salesPerMonth(dataSet = []) {
            var valueFormatString = "DD MMM";
            if ($('#multiDrop2').val() == "0") {
                valueFormatString = "MMM";
            }
            var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true,
                title: {
                    text: headText,
                    fontSize: 14
                },
                axisX: {
                    valueFormatString: valueFormatString,
                    crosshair: {
                        enabled: true,
                        snapToDataPoint: true
                    }
                },
                axisY: {
                    title: "Amount (USD)",
                    includeZero: false,
                    valueFormatString: "##0",
                    crosshair: {
                        enabled: true,
                        snapToDataPoint: true,
                        labelFormatter: function (e) {
                            return "" + CanvasJS.formatNumber(e.value, "##0");
                        }
                    }
                },
                data: [{
                        type: "splineArea",
                        xValueFormatString: "DD MMM",
                        yValueFormatString: "##0",
                        color: "#DEEBAC",
                        dataPoints: dataSet
                    }]
            });
            console.log(chart);

            chart.render();

        }


        function loadCategory(data) {
            var chart = new CanvasJS.Chart("chartContainer1", {
                animationEnabled: true,

                title: {
                    text: "Total Expense on Categories"
                },
                axisX: {
                    interval: 1
                },
                axisY2: {
                    interlacedColor: "rgba(1,77,101,.2)",
                    gridColor: "rgba(1,77,101,.1)",
                    title: "Amount (USD)"
                },
                data: [{
                        type: "bar",
                        name: "companies",
                        axisYType: "secondary",
                        color: "#014D65",
                        dataPoints: data
                    }]
            });
            chart.render();

        }


        function addDropdown() {
            var multiYears = ["2015", "2016", "2017", "2018", "2019", "2020", "2021"];
            var monthNames = ["All", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            var htmlOptions, htmlOptions2 = "";
            var i;


            var currentDate = new Date();

            for (m = 0; m < multiYears.length; m++) {
                var multiYear = multiYears[m];
                var startMonth = 1;
                // if it's the current year, only show from this month onwards
                if (multiYear == currentDate.getYear() + 1900) {
                    startMonth = currentDate.getMonth() + 1;
                }

                htmlOptions2 += '<option onchange="analytics()" value="' + multiYears[m] + '">' + multiYears[m] + '</option>';
            }

            for (i = startMonth; i <= 13; i++) {
                htmlOptions += '<option value="' + (i - 1) + '">' + monthNames[i - 1] + '</option>';
            }

            var htmlStart = 'Year : <select onchange="analytics()" size="1" id="multiDrop" name="multiDrop">';
            var htmlOptionsHeader = '<option value="_" disabled>Select Year</option>';
            var htmlEnd = '</select>';
            var html = htmlStart + htmlOptionsHeader + htmlOptions2 + htmlEnd;

            document.getElementById("year-month").innerHTML += html;

            var htmlStart = '&nbsp;&nbsp;&nbsp;Month : <select onchange="analytics()" size="1" id="multiDrop2" name="multiDrop">';
            var htmlOptionsHeader = '<option value="_" disabled>Select Month</option>';
            var htmlEnd = '</select>';
            var html = htmlStart + htmlOptionsHeader + htmlOptions + htmlEnd;

            document.getElementById("year-month").innerHTML += html;



        }

        function round(num) {
            return Math.round(num * 100) / 100;
        }

        function lib() {

            addDropdown();

            $('#multiDrop').val(new Date().getFullYear());
            $('#multiDrop2').val(new Date().getMonth());
            analytics();
        }

        document.addEventListener("DOMContentLoaded", lib);
    </script>

