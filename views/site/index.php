<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
   <div class="login-home">
      <div class="container">
        <div class="login-content">
          <div class="location">
            <img src="/images/blank.png" alt="APP LOGO" />
          </div>

          <div class="login-welcome">
            <h1>Hey, good to see you again!</h1>
            <h5>Enter your email address and password to get going</h5>
          </div>

          <div class="login-form">
            <div class="registration-form">
              <div>
                <div class="form-group">
                  <label>Email Address</label>
                  <input
                    type="email"
                    class="form-control"
                    id="loginEmail"
                    required=""
                    onkeypress="(event.keyCode === 13)?login():false;"
                  />
                </div>
                <div class="form-group">
                  <label>Password</label>
                  <input
                    type="password"
                    class="form-control"
                    id="loginPassword"
                    required=""
                  />
                </div>

                <button
                  type="submit"
                  id="submitBtn"
                  onclick="login()"
                  class="sign-in"
                >
                  Sign In
                </button>

                <div class="forgot">
                  <a href="/site/register">Register New user?</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php require_once(__DIR__ . '/../layouts/alerts/error.php'); ?>
<script>

function login() {
  var jsonData = {
    emailId: document.getElementById("loginEmail").value,
    password: document.getElementById("loginPassword").value,
  };

  //header =request.overrideMimeType("application/j-son;charset=UTF-8");

  $.ajax({
    type: "POST",
    beforeSend: function (request) { },
    url: API_URL + "v1/userLogin",
    dataType: "json",
    data: JSON.stringify(jsonData),
    contentType: "application/json",
    timeout: 8000,
    success: function (data) {
      //console.log(data);
      if (data.code == 200) {

        saveLoginDetails(data);
        localStorageSAVE("_newSession", "1");
      } else {
        ERRMSG("User Login",data.errorMessage);
      }
    },
    error: function (x, t, m) {
      if (t === "timeout") {
        consoleWarn("WARN : ", "Server did not respond. Please try later.");
      }
    }
  });

}
</script>
