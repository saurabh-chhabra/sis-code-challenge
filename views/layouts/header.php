<nav id="sidebar" class="sidebar-wrapper">
      <div class="sidebar-content">
        <div class="sidebar-brand">
          <a href="#"><img src="/images/blank.png">APP Title</a>
          <div id="close-sidebar">
          <!-- <i class="fa fa-angle-left" aria-hidden="true"></i> -->
          <img src="/images/sidebar-arrow.svg">
          </div>
        </div>

        <div class="sidebar-header">
          <div class="side-user-photo">
             <img src="/images/avatar.png" alt="">
          </div>
          <div class="user-info">
            <span class="user-name"></span>
            <!-- <span class="user-role">Administrator
          </span> -->
          </div>
      
        </div>

        <!-- sidebar-search  -->
        <div class="sidebar-menu">
          <ul>
            <!-- <li class="header-menu">
            <span>General</span>
          </li> -->
          <li class="sidebar-dropdown">
              <a href="/site/dashboard">
                <i class="fa fa-th-large" aria-hidden="true"></i>
                <span>Dashboard</span>
                <!-- <span class="badge badge-pill badge-warning">New</span> -->
              </a>
             
            </li>
            <li class="sidebar-dropdown admin">
              <a href="/site/upload">
              <i class="fa fa-user" aria-hidden="true"></i>
                <span>Upload Expense Sheet</span>
                <!-- <span class="badge badge-pill badge-warning">New</span> -->
              </a>
              
            </li>
            <li class="sidebar-dropdown admin" >
              <a href="/site/user">
                <i class="fa fa-briefcase" aria-hidden="true"></i>
                <span>User List</span>
                <!-- <span class="badge badge-pill badge-danger">3</span> -->
              </a>
            </li>
            <li class="sidebar-dropdown">
              <a href="/site/expense">
                <i class="fa fa-briefcase" aria-hidden="true"></i>
                <span>Expense List</span>
                <!-- <span class="badge badge-pill badge-danger">3</span> -->
              </a>
            </li>
             <li class="sidebar-dropdown">
              <a href="javascript:void(0);" onclick="logout()">
                <i class="fa fa-globe"></i>
                <span>Logout</span>
              </a>
            </li>

          </ul>
        </div>
        <!-- sidebar-menu  -->
      </div>

    </nav>
<script>
function ready(){
    var roleName = localStorage.getItem("roleName");
    $(".user-name").html("Welcome, "+localStorage.getItem("fullName"));  
    
    if(roleName=="USER"){
        $(".admin").css('display','none');
    }else{
         $(".admin").css('display','block');
    }
}

 document.addEventListener("DOMContentLoaded", ready);
</script>