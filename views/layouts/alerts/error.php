<!-- Modal -->
<div class="modal fade" id="error-alert" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">
          &times;
        </button> -->
        <h4 class="modal-title errorTitle">-</h4>
      </div>
      <div class="modal-body">
        <div class="form-section">
          <div class="registration-form">
            <div name="register-form">

              <div class="form-group img">
                <img src="/images/error.png"/>
              </div>
             
              <div class="form-group">
                <div class="errorMessage">-</div>
              </div>
              
              <div class="form-group">
                <button
                  type="submit"
                  class="login forgot-btn btn btn-danger btn-fw"
                  data-dismiss="modal"
                >
                  Ok
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
