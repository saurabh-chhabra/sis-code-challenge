<!-- Modal -->
<div class="modal fade" id="confirm" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">
          &times;
        </button> -->
        <h4 class="modal-title successTitle">Confirm</h4>
      </div>
      <div class="modal-body">
        <div class="form-section">
          <div class="registration-form">
            <div name="register-form">

              <div class="form-group img">
                <img src="/images/success.png"/>
              </div>
             
              <div class="form-group">
                <div class="successMessage">Are you sure you want to perform the action ?</div>
              </div>
              
              <div class="form-group">
                <button
                  type="submit"
                  class="login forgot-btn btn btn-outline-warning"
                  data-dismiss="modal"
                >
                  Cancel
                </button>
              </div>
              <div class="form-group">
                <button
                  type="submit"
                  class="login forgot-btn btn btn-info btn-fw"
                  onclick="modalConfirm()"
                >
                  Ok
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
