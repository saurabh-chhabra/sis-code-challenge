var API_URL = "http://localhost:9191/";
var allUserdata = [];
var userdata = {};
var uploadURL = "";

$(document).ready(function () {
    retryAJAX();

    setTimeout(function () {
        if (getParam("loader") == "on") {
        } else {
            $(".se-pre-con").fadeOut("slow");
        }
    }, 1000);
    $($("a[href*='" + window.location.pathname + "']").closest(".sidebar-dropdown")).addClass(
            "active-menu"
            );

    // $($("a[href*='" + window.location.pathname + "']").closest(".sidebar-dropdown-ul")).addClass(
    //   "active-submenu"
    // );

    // $($("a[href*='" + window.location.pathname + "']").parent(".submenu")).addClass(
    //   "active-submenu"
    // );

    loadSidebarAnimation();
});

function loader() {
    return {
        top: function top() {
            NProgress.start();
        },
        center: function center() {}
    };
}

function hideloader() {
    return {
        top: function top() {
            NProgress.done();
        },
        center: function center() {}
    };
}

function getParam(param) {
    return new URLSearchParams(window.location.search).get(param);
}

var saveLoginDetails = function (data, isRegister = 0) {


    localStorage.setItem("refreshToken", data.data.refreshToken);
    localStorage.setItem("roleName", data.data.profileObject.profile.roleName);
    localStorage.setItem("fullName", data.data.fullName);
    localStorage.setItem("sessionToken", data.data.sessionToken);
    localStorage.setItem(
            "userMasterId",
            data.data.profileObject.profile.userId
            );
    //saveUseData(data);
    window.location.href = "/site/dashboard";
};

var consolelog = function (key = "->", value) {
    console.log(key, value);
};

var consoleTBL = function (_jsonOBJ) {
    console.table(_jsonOBJ);
};

var consoleWarn = function (key = "->", value) {
    console.warn(key, value);
};

var localStorageSAVE = async function (key, value) {
    localStorage.setItem(key, value);
};

var localStorageGET = function (key) {
    if (localStorage.getItem(key) == null) {
        return null;
    }
    return localStorage.getItem(key);
};

var localStorageDEL = function (key) {
    return localStorage.removeItem(key);
};

var parseJSON = function (json) {
    return JSON.parse(json);
};

var stringifyJSON = function (json) {
    return JSON.stringify(json);
};

var isObject = function (val) {
    return typeof val === "object";
};

var ISNULL = function (str, replaceSign = "-") {
    if (str == null) {
        return replaceSign;
    }
    return str;
};

var SUCCESSMSG = function (title, message) {
    //alert(message);
    $(".successTitle").html(title);
    $(".successMessage").html(message);
    $("#success-alert").modal();
};

var ERRMSG = function (title, message) {
    //alert(message);
    $(".errorTitle").html(title);
    $(".errorMessage").html(message);
    $("#error-alert").modal();
};

function retryAJAX() {
    try {
        // Progress Loader
        //NProgress.start();

        var retries = 3;
        var retryInterval = 500;
        //ajax retry
        $.ajax = ($oldAjax => {
            // on fail, retry by creating a new Ajax deferred
            function check(a, b, c) {
                var shouldRetry = b != "success" && b != "parsererror";
                if (a.code == 440) {
                    logout();
                    //$("#user-login").modal();
                    return;
                } else {
                    if (shouldRetry && --retries > 0) {
                        //console.log(retries);
                        setTimeout(() => {
                            $.ajax(this);
                        }, retryInterval || 100);
                    }
                }
            }

            return settings => $oldAjax(settings).always(check);
        })($.ajax);

        $(document).ajaxSend(function () {
            consoleWarn("AJAX", "Calling of function");
            loader().top();
        });
        $(document).ajaxComplete(function () {
            consoleWarn("AJAX", "Ending of function");
            hideloader().top();
        });
    } catch (error) {
        consoleWarn(error);
    }
}

var logout = function () {
    window.localStorage.clear();
    window.location.href = "/";
};

var encodeImageFileAsURL = function (element, type = 0) {
    var file = element.files[0];
    var reader = new FileReader();
    reader.onloadend = function () {
        //console.log('RESULT', reader.result);
        upload(reader.result, type);
    };
    reader.readAsDataURL(file);
};

var upload = function (base64, type = 0) {
    try {
        var refreshToken = localStorage.getItem("refreshToken");
        var sessionToken = localStorage.getItem("sessionToken");
        var userId = localStorage.getItem("userMasterId");

        var imageObj = {
            image: base64
        };

        $.ajax({
            type: "POST",
            beforeSend: function (request) {
                request.setRequestHeader("userId", userId);
                request.setRequestHeader("refreshToken", refreshToken);
                request.setRequestHeader("sessionToken", sessionToken);
            },
            url: API_URL + "v1/baseUpload?",
            data: JSON.stringify(imageObj),
            dataType: "json",
            contentType: "application/json",
            timeout: 600000,
            success: function (data) {
                if (data.code == 200) {
                    uploadURL = data.url;
                    $(".upload-img-attr").attr("src", base64);
                }
            },
            error: function (x, t, m) {
                if (t === "timeout") {
                    consoleWarn("WARN : ", "Server did not respond. Please try later.");
                }
            }
        });
    } catch (error) {
        console.log(error);
}
};

var dateFormat = (function () {
    var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
            timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
            timezoneClip = /[^-+\dA-Z]/g,
            pad = function (val, len) {
                val = String(val);
                len = len || 2;
                while (val.length < len)
                    val = "0" + val;
                return val;
            };

    // Regexes and supporting functions are cached through closure
    return function (date, mask, utc) {
        var dF = dateFormat;

        // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
        if (
                arguments.length == 1 &&
                Object.prototype.toString.call(date) == "[object String]" &&
                !/\d/.test(date)
                ) {
            mask = date;
            date = undefined;
        }

        // Passing date through Date applies Date.parse, if necessary
        date = date ? new Date(date) : new Date();
        if (isNaN(date)) {
            return date;
        }

        mask = String(dF.masks[mask] || mask || dF.masks["default"]);

        // Allow setting the utc argument via the mask
        if (mask.slice(0, 4) == "UTC:") {
            mask = mask.slice(4);
            utc = true;
        }

        var _ = utc ? "getUTC" : "get",
                d = date[_ + "Date"](),
                D = date[_ + "Day"](),
                m = date[_ + "Month"](),
                y = date[_ + "FullYear"](),
                H = date[_ + "Hours"](),
                M = date[_ + "Minutes"](),
                s = date[_ + "Seconds"](),
                L = date[_ + "Milliseconds"](),
                o = utc ? 0 : date.getTimezoneOffset(),
                flags = {
                    d: d,
                    dd: pad(d),
                    ddd: dF.i18n.dayNames[D],
                    dddd: dF.i18n.dayNames[D + 7],
                    m: m + 1,
                    mm: pad(m + 1),
                    mmm: dF.i18n.monthNames[m],
                    mmmm: dF.i18n.monthNames[m + 12],
                    yy: String(y).slice(2),
                    yyyy: y,
                    h: H % 12 || 12,
                    hh: pad(H % 12 || 12),
                    H: H,
                    HH: pad(H),
                    M: M,
                    MM: pad(M),
                    s: s,
                    ss: pad(s),
                    l: pad(L, 3),
                    L: pad(L > 99 ? Math.round(L / 10) : L),
                    t: H < 12 ? "a" : "p",
                    tt: H < 12 ? "am" : "pm",
                    T: H < 12 ? "A" : "P",
                    TT: H < 12 ? "AM" : "PM",
                    Z: utc
                            ? "UTC"
                            : (String(date).match(timezone) || [""])
                            .pop()
                            .replace(timezoneClip, ""),
                    o:
                            (o > 0 ? "-" : "+") +
                            pad(Math.floor(Math.abs(o) / 60) * 100 + (Math.abs(o) % 60), 4),
                    S: ["th", "st", "nd", "rd"][
                            d % 10 > 3 ? 0 : (((d % 100) - (d % 10) != 10) * d) % 10
                    ]
                };

        return mask.replace(token, function ($0) {
            return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
        });
    };
})();

// Some common format strings
dateFormat.masks = {
    default: "ddd mmm dd yyyy HH:MM:ss",
    shortDate: "m/d/yy",
    mediumDate: "mmm d, yyyy",
    longDate: "mmmm d, yyyy",
    fullDate: "dddd, mmmm d, yyyy",
    shortTime: "h:MM TT",
    mediumTime: "h:MM:ss TT",
    longTime: "h:MM:ss TT Z",
    isoDate: "yyyy-mm-dd",
    isoTime: "HH:MM:ss",
    isoDateTime: "yyyy-mm-dd'T'HH:MM:ss",
    isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
    dayNames: [
        "Sun",
        "Mon",
        "Tue",
        "Wed",
        "Thu",
        "Fri",
        "Sat",
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"
    ],
    monthNames: [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec",
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
    ]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
    return dateFormat(this, mask, utc);
};

function formatDate(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? "pm" : "am";
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? "0" + minutes : minutes;
    var strTime = hours + ":" + minutes + " " + ampm;
    return (
            date.getMonth() +
            1 +
            "/" +
            date.getDate() +
            "/" +
            date.getFullYear() +
            " " +
            strTime
            );
}

var openModal = function (id) {
    $("#" + id).modal();
};

function loadSidebarAnimation() {
    $(".sidebar-dropdown > a").click(function () {
        $(".sidebar-submenu").slideUp(200);
        if (
                $(this)
                .parent()
                .hasClass("active")
                ) {
            $(".sidebar-dropdown").removeClass("active");
            $(this)
                    .parent()
                    .removeClass("active");
        } else {
            $(".sidebar-dropdown").removeClass("active");
            $(this)
                    .next(".sidebar-submenu")
                    .slideDown(200);
            $(this)
                    .parent()
                    .addClass("active");
        }
    });

    $("#close-sidebar").click(function () {
        $(".page-wrapper").removeClass("toggled");
    });
    $("#show-sidebar").click(function () {
        $(".page-wrapper").addClass("toggled");
    });
}


